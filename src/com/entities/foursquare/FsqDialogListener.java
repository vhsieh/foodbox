package com.entities.foursquare;

public interface FsqDialogListener {
	public abstract void onComplete(String accessToken);
	public abstract void onError(String error);
}
