package com.entities.foursquare;

public interface FsqAuthListener {
	public abstract void onSuccess();
	public abstract void onFail(String error);
}