package com.entities;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.custom.CommentAdapter;
import com.custom.ToEatListDialogFragment;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.functionalities.DrawableBackgroundDownloader;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.utility.foodbox.MapActivity;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;
import com.utility.foodbox.HomeFragment;
import com.utility.foodbox.ImageContainer;
import com.utility.foodbox.R;

@SuppressLint("NewApi")
public class RestaurantFragment extends Fragment{
	public static final String ARG = "restaurant";
	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader(); 
	public int current = 0;
	private CommentAdapter commentAdapter = null;
	private ArrayList<Comment> comments = new ArrayList<Comment>();
	private Restaurant r = null;
	private Handler mHandler = new Handler(Looper.getMainLooper());
	private int currentCount = 0;
	private HashMap<String, String> peddingPhoto = new HashMap<String, String>();
	//View Flipper parameters
    //private ViewFlipper mViewFlipper;
    
    private Context mContext = null;
    private int screen_width;
    private int screen_height;
    private View rootView;
    /*
     * 
     * view ininitialization
     */
    ListView comment_listview;
    EditText comment;
    ImageView comment_profile;
    View header;
    View footer;
    ImageView trans;
    CheckBox notification;
    Button publish;
    TextView phone;
    TextView checkins;
    TextView address;
    TextView distance;
    TextView website;
    ImageView cover;
    TextView prompt;
    TextView more_pictures;
    Button add;
    /*
     * Map parameters
     */
    MapView mMapView;
    GoogleMap mGoogleMap;
    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static RestaurantFragment newInstance(int num) {
    	RestaurantFragment f = new RestaurantFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", num);
        f.setArguments(args);

        return f;
    }
    @Override
    public void onAttach(Activity activity){
    	super.onAttach(activity);
    	Log.d(ARG, "onAttach called");
    }
    /*
     * basic setting except creating views
     * 
     */
    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        getActivity().getActionBar().hide();
    	getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    	setScreenSize();
    	mContext = getActivity().getApplicationContext();
    	current = getArguments() != null ? getArguments().getInt("position") : 0;
    	this.r = Global.restaurants.get(current);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    
    	// inflate the root view
    	if (rootView == null) {
	    	rootView = inflater.inflate(R.layout.single_restaurant, container, false);
	    	/*Bundle args = getArguments();
	    	
	    	r = (Restaurant)args.getSerializable("Restaurant");
	    	*/
	    	//Get comments for this restaurant
	    	comment_listview = (ListView)rootView.findViewById(R.id.comments);
	    	
	    	//set the information area as the header of ListView for comments
	    	header = View.inflate(getActivity().getApplicationContext(), R.layout.info_area, null);
	    	//set transparency image layout_width and layout_height
	    	trans = (ImageView) header.findViewById(R.id.transparency_image);
	    	trans.setLayoutParams(new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(screen_width, (int) ((double)screen_height*0.4))));
	    	comment_listview.addHeaderView(header);
	    	//set on touch event to handle image
	    	
	    	header.setOnClickListener(new View.OnClickListener() {
	    				
	    				@Override
	    				public void onClick(View v) {
	    					// TODO Auto-generated method stub
	    					showFullScreenImage();
	    				}
	    	});
	    	
	    	cover = (ImageView) rootView.findViewById(R.id.single_cover_image);
	    	cover.setLayoutParams(new FrameLayout.LayoutParams(new FrameLayout.LayoutParams((int) (this.screen_width), (int) ((double)this.screen_height*0.4))));
	    	prompt = (TextView)rootView.findViewById(R.id.imagetitle);
	    	prompt.setText("0");
	    	more_pictures = (TextView) rootView.findViewById(R.id.more_pictures);
	    	setupCover();
	    	
	    	
	    	// handle the information area
	    	phone = (TextView)header.findViewById(R.id.phone_number);
	    	phone.setText(this.r.getTEL());
	    	phone.setMaxLines(2);
	    	Linkify.addLinks(phone, Linkify.PHONE_NUMBERS);
	    			
	    	checkins = (TextView)header.findViewById(R.id.store_checkins);
	    	checkins.setText(String.valueOf(this.r.getCheckin())+" ���d");
	    	checkins.setMaxLines(2);
	    			
	    	address = (TextView)header.findViewById(R.id.store_address);
	    	address.setText(this.r.getAddress());
	    	address.setMaxLines(2);
	    	Linkify.addLinks(address, Linkify.ALL);
	    	//add event
	    	address.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(getActivity(), MapActivity.class);
					i.putExtra("lat", Double.valueOf(r.getLatitude()));
					i.putExtra("lng", Double.valueOf(r.getLongitude()));
					i.putExtra("title", r.getTitle());
					startActivity(i);
				}
			});
	    	
	    	distance = (TextView)header.findViewById(R.id.distance);
	    	distance.setText("�Z��"+String.valueOf(this.r.getDistance())+" km");
	    	distance.setMaxLines(2);
	    	
	    	website = (TextView)header.findViewById(R.id.website);
	    	website.setText(this.r.getWebsite());
	    	website.setMaxLines(2);
	    	Linkify.addLinks(website, Linkify.WEB_URLS);
	    	
	    	add = (Button)header.findViewById(R.id.add_to_eat_list);
	    	new Thread(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					checkExistedToEatList();
				}
	    		
	    	}).start();
	    	addToEatList();
	    	
	    	publish = (Button)header.findViewById(R.id.publish);
	    	footer = View.inflate(mContext, R.layout.loading_more, null);
	    	setupComment();// set up comment function
	    	
	    	/*
	    	 * checkBox to register a notification event when user approachs.
	    	 */
	    	
	    	notification = (CheckBox) header.findViewById(R.id.notification);
	    	setupNotification();
	    	
        }else{
        	ViewGroup parent = (ViewGroup) rootView.getParent();
            parent.removeView(rootView);
        }
    	
    	return rootView;
    }
    /*
     * Check whether current restaurant belong to a existed to_eat_list or not
     */
    private void checkExistedToEatList(){
    	ParseQuery<ParseObject> query = ParseQuery.getQuery("to_eat_list");
    	query.whereEqualTo("user", ParseUser.getCurrentUser());
    	query.whereEqualTo("object_ids", this.r.getObject_id());
    	query.findInBackground(new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> response, ParseException e) {
				// TODO Auto-generated method stub
				Log.d(ARG, response.toString());
				if(e==null){ // something existed in Parse
					if(response.size()>0){
						ParseObject obj = response.get(0);
						final String group = obj.getString("group");
						mHandler.post(new Runnable(){

							@Override
							public void run() {
								// TODO Auto-generated method stub
								add.setText("@"+group);
								add.setBackgroundColor(getActivity().getResources().getColor(R.color.silver));
								add.setTextColor(getActivity().getResources().getColor(R.color.black));
							}
							
						});
					}
				}
			}
    		
    	});
    	
    }
    /*
     * handle the action of "to-eat-list" add button 
     */
    private void addToEatList(){
    	
    	add.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				add.setBackgroundColor(getResources().getColor(R.color.light_red));
				List<String> to_eat_list = new ArrayList<String>();
				@SuppressWarnings("unchecked")
				List<String> existed_list = (List<String>) ParseUser.getCurrentUser().get("to_eat_list");
				ArrayList<String> distinct = new ArrayList<String>(new HashSet<String>(existed_list));
				if(distinct!=null)
					to_eat_list.addAll(distinct);
				if(to_eat_list!=null){
					DialogFragment toEatList = new ToEatListDialogFragment(to_eat_list, r.getTitle(), r.getObject_id());
					toEatList.setCancelable(true);
					toEatList.show(getFragmentManager(), ARG);
				}
			}
		});
    }
    /*
	 * checkBox to register a notification event when user approachs.
	 */
    private void setupNotification(){
    	notification.setChecked(false);// default set to unscribed!
    	//Check if current restaurant is subscribed by current user
    	Set<String> setOfAllSubscriptions = PushService.getSubscriptions(this.getActivity().getApplicationContext());
    	
    	if(setOfAllSubscriptions.contains("a"+this.r.getObject_id())){
    		notification.setChecked(true);
    	}
    	notification.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					// When users indicate they want getting notification when approaching here
					//, we subscribe them to that channel.
					ParseInstallation installation = ParseInstallation.getCurrentInstallation();
			    	installation.put("user", ParseUser.getCurrentUser());
					installation.saveInBackground();
					PushService.subscribe(getActivity().getApplicationContext(), "a"+r.getObject_id(), HomeFragment.class);
				}else{
					PushService.unsubscribe(getActivity().getApplicationContext(), "a"+r.getObject_id());
				}
			}
    		
    	});
    }
    /*
     * set up all the functions about comment.
     */
    public void setupComment(){
    	commentAdapter = new CommentAdapter(mContext, R.id.comments, comments);
    	comment_listview.setAdapter(commentAdapter);
    			
    	if(this.r.getComment()!=null){
    		final ArrayList<Comment> items = this.r.getComment();
    		
    		if(items.size()>5){
    			
    			comment_listview.addFooterView(footer);
    			comments.addAll(items.subList(currentCount, currentCount+5));
    			currentCount+=5;
    			commentAdapter.addItems(comments);
    			commentAdapter.notifyDataSetChanged();
    			//set the on click event
    			Button load_more = (Button)footer.findViewById(R.id.loading_more);
    			load_more.setOnClickListener(new View.OnClickListener() {
    						
    				@Override
    				public void onClick(View v) {
    					// TODO Auto-generated method stub
    					int end = currentCount+5 >= items.size()? items.size()-1:currentCount+5;
   						List<Comment> dataToUpdate = items.subList(currentCount, end);
    					currentCount = end;
    					if(end==items.size()-1){
    						comment_listview.removeFooterView(footer);
    					}
    					comments.addAll(dataToUpdate);
    					commentAdapter.addItems(comments);
    					commentAdapter.notifyDataSetChanged();
    				}
    			});
    		}else{
    			comments.addAll(items);
    			commentAdapter.addItems(comments);
    			commentAdapter.notifyDataSetChanged();
    		}
    	}
    	//Set up "write a comment" section
    	comment = (EditText)header.findViewById(R.id.add_new_comment);
    	//set up profile of current user
    	comment_profile = (ImageView)header.findViewById(R.id.comment_profile);
    	
    	//float newWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, this.getResources().getDisplayMetrics());
    	
    	//new ImageDownloaderWithCache(this.mContext, newWidth).download(Global.iconUrl, comment_profile);
    	Drawable d = this.getActivity().getResources().getDrawable(R.drawable.downloading);
    	downloader.loadDrawable(Global.iconUrl, comment_profile, d);		
    	//define click event of "add new comment"
    			
    	LayoutParams params=comment.getLayoutParams();
    	params.width=(int) (screen_width-(int)50* this.getResources().getDisplayMetrics().density);
    	comment.setLayoutParams(params);
    	
    	
    	comment.setOnTouchListener(new View.OnTouchListener() {
    		
    		@Override
    		public boolean onTouch(View v, MotionEvent event) {
    			// TODO Auto-generated method stub
    			comment.requestFocus();
    	        comment.setFocusableInTouchMode(true);
    	        comment.setText("");
    	        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
    	        imm.showSoftInput(comment, InputMethodManager.SHOW_FORCED);
    			publish.setVisibility(View.VISIBLE);
    					
    			return false;
    		}
    	});
    	publish.setOnClickListener(new View.OnClickListener() {
    				
    		@Override
    		public void onClick(View v) {
    			// TODO Auto-generated method stub
    			// has some texts
    			if(comment.getText().toString()!=""){
    				//we first hide the virtual keyboard
    				ProgressBar pg = (ProgressBar)rootView.findViewById(R.id.loading_text);
    				pg.setVisibility(View.VISIBLE);
    				pg.bringToFront();
    				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
    				imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    				comment.setFocusableInTouchMode(false);
    				//set the comment
    						
    				comment_listview.setSelection(commentAdapter.getCount()-1);// set selection to the last
    						
    				Comment newComment = new Comment(comment.getText().toString(), r.getTitle(), Global.iconUrl, Global.fbName);
    				comment.setText("");//clear up the edit text
    				saveCommentToParse(newComment, rootView);
    						
    			}
    		}
    	});
    }
    @Override
    public void onResume(){
    	//Check if current restaurant is subscribed by current user
    	Set<String> setOfAllSubscriptions = PushService.getSubscriptions(this.getActivity().getApplicationContext());
    	
    	if(setOfAllSubscriptions.contains("a"+this.r.getObject_id())){
    		notification.setChecked(true);
    	}
    	/*
        MockLocationProvider mock = new MockLocationProvider(LocationManager.NETWORK_PROVIDER, getActivity().getApplicationContext());
      	//Set test location
    	mock.pushLocation(Double.valueOf(this.r.getLatitude()), Double.valueOf(this.r.getLongitude()));
    	*/
    	Log.d(ARG, "onResume called");
    	super.onResume();
    }
    
    @Override
    public void onPause(){
    	Log.d(ARG, "onPause called");
    	super.onPause();
    }
    
    @Override
    public void onStop(){
    	Log.d(ARG, "onStop called");
    	super.onStop();
    }
    @Override
    public void onDestroy(){
    	super.onDestroy();
    	//comment_profile.getDrawable().setCallback(null);
    	//trans.getDrawable().setCallback(null);
    	Log.d(ARG, "onDestroy called");
    }
    @SuppressWarnings("deprecation")
	private void setScreenSize(){
    	//get screen width
    	Display display = getActivity().getWindowManager().getDefaultDisplay(); 

    	this.screen_width = display.getWidth();
    	this.screen_height = display.getHeight();
    }

	@SuppressWarnings("deprecation")
	protected float getScreenWidth(){
		float Measuredwidth = 0;
		Display display = getActivity().getWindowManager().getDefaultDisplay();
	    DisplayMetrics outMetrics = new DisplayMetrics();
	    float density  = getResources().getDisplayMetrics().density;
	    
	    display.getMetrics(outMetrics);
	    
		Point size = new Point();
		WindowManager w = getActivity().getWindowManager();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
		{
		    w.getDefaultDisplay().getSize(size);

		    Measuredwidth = size.x/density;
		}
		else
		{
		    Display d = w.getDefaultDisplay();
		    Measuredwidth = d.getWidth()/density;
		}
		return Measuredwidth;
	}
	
	private void saveCommentToParse(final Comment comment, final View rootView){
		
		//now, we relate the restaurant and comment
		ParseQuery<ParseObject> query = ParseQuery.getQuery("restaurant");
		query.whereEqualTo("name", comment.getRestaurantTitle());
		query.getFirstInBackground(new GetCallback<ParseObject>(){

			@Override
			public void done(ParseObject data, ParseException e) {
				// TODO Auto-generated method stub
				if(e==null){
					ParseObject comment_po = new ParseObject("comment");
					comment_po.put("comment", comment.getComment());
					comment_po.put("restaurant", data);
					comment_po.put("icon_url", comment.getIconUrl());
					comment_po.put("userID", ParseUser.getCurrentUser().getObjectId());
					comment_po.put("user_name", Global.fbName);
					data.add("comments", comment_po);
				}
				data.saveInBackground(new SaveCallback(){

					@Override
					public void done(ParseException e) {
						// TODO Auto-generated method stub
						if(e==null){
							//call main UI thread to update the comment
							mHandler.post(new Runnable(){

								@Override
								public void run() {
									// TODO Auto-generated method stub
									//comments.clear();
									comments.addAll(fetchNewComments(comment.getRestaurantTitle()));
									//commentAdapter.clear();
									commentAdapter.addItems(comments);
									//hide the progress bar
									ProgressBar pg = (ProgressBar)rootView.findViewById(R.id.loading_text);
									pg.setVisibility(View.GONE);
									commentAdapter.notifyDataSetChanged();
									ListView comment_listview = (ListView)rootView.findViewById(R.id.comments);
									comment_listview.setSelection(commentAdapter.getCount()-1);
								}
							});
							
						}else{
							e.printStackTrace();
						}
					}
					
				});
			}
			
		});
	}
	private ArrayList<Comment> fetchNewComments(String Title){
		ArrayList<Comment> updatedComments = new ArrayList<Comment>();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("restaurant");
		query.whereEqualTo("name", Title);
		ParseObject restaurantObject=null;
		try {
			restaurantObject = query.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<ParseObject> commentObject = restaurantObject.getList("comments");
		
		int currentNumerOfItems = commentAdapter.getCount();
		
		for(int i=currentNumerOfItems;i<commentObject.size();i++){
			ParseObject p = commentObject.get(i);
			Comment c = null;
			try {
				String userName = Global.fbName;
				c = new Comment(p.fetchIfNeeded().getString("comment"), Title, p.fetchIfNeeded().getString("icon_url"), userName);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			updatedComments.add(c);
		}
	
		return updatedComments;
	}
	/*
	 * 
	 */
	public void setupCover(){
		if(r.getCoverUrl().length()>0){ // we show the cover image if any
			Drawable d = this.getActivity().getResources().getDrawable(R.drawable.download_in_process);
	    	downloader.loadDrawable(r.getCoverUrl(), cover, d);
		}
			//AddImageToCover(r.getCoverUrl());
	    if(r.getID().length()>0) // we then display number of images that friends checkin here to the user
	    	  GetMoreImageByID(r.getID());
	}
	//Get more image from page's album from FB
	private void GetMoreImageByID(String ID){
		
		//String fqlQuery = "SELECT src FROM photo WHERE aid IN (Select aid from album where owner = "+ID+") limit 5";
		//String fqlQuery = "select src from photo where owner IN (select uid2 from friend where uid1=me()) and place_id = "+ID+" limit 5";
		String fqlQuery = "select app_data.photo_ids  from stream where post_id  IN (select post_id from location_post where page_id = "+ID+")";
		Bundle param = new Bundle();
		param.putString("q", fqlQuery);
		Request request = new Request(Session.getActiveSession(), "/fql", param, HttpMethod.GET, new Request.Callback() {
			@Override
			public void onCompleted(Response response) {
				// TODO Auto-generated method stub
				FacebookRequestError error = response.getError();
				if(error==null){
					GraphObject graphObject = response.getGraphObject();
					if(graphObject !=null){
						JSONObject obj = graphObject.getInnerJSONObject();
						JSONArray data = obj.optJSONArray("data");
						if(data.length()>0){
							for(int i=0;i<data.length();i++){
								final JSONObject entity = (JSONObject) data.opt(i);
								JSONObject app_data = entity.optJSONObject("app_data");
								if(app_data!=null){
									JSONArray photo_ids = app_data.optJSONArray("photo_ids");
									for(int j=0;j<photo_ids.length();j++){
										GetPhotoByPhotoID(photo_ids.optString(j));
									}
								}
							}
					    }
					}
				}else{System.out.println(error.toString());}
			}
		});
		request.executeAsync();
	}
	private void GetPhotoByPhotoID(String photo_id){
		/* make the API call */
		Log.d(ARG, photo_id);
		new Request(
		    Session.getActiveSession(),
		    "/"+photo_id,
		    null,
		    HttpMethod.GET,
		    new Request.Callback() {
		        public void onCompleted(Response response) {
		            /* handle the result */
		        	FacebookRequestError error = response.getError();
		        	if(error==null){
		        		GraphObject graphObject = response.getGraphObject();
		        		if(graphObject !=null){
		        			JSONObject obj = graphObject.getInnerJSONObject();
		        			//Get the image url
		        			JSONArray images = obj.optJSONArray("images");
		        			JSONObject image = images.optJSONObject(0);
		        			String url = image.optString("source");
		        			
		        			//Get the owner
		        			JSONObject owner_obj = obj.optJSONObject("from");
		        			String owner = owner_obj.optString("name");
		        			int currentNum = Integer.valueOf(prompt.getText().toString())+1;
		        			prompt.setText(String.valueOf(currentNum));
		        			prompt.setVisibility(View.VISIBLE);
		        			more_pictures.setVisibility(View.VISIBLE);
		        			peddingPhoto.put(url, owner);
		        			
		        		}
		        	}
		        }
		    }
		).executeAsync();
	}
	
	/*
	 * work when user click a single image, then display the full-screen size image
	 */
	private void showFullScreenImage(){
		Intent i = new Intent(this.getActivity(), ImageContainer.class);
		ArrayList<String> urls = new ArrayList<String>();
		ArrayList<String> owners = new ArrayList<String>();
		for(String key:peddingPhoto.keySet()){
			String owner = peddingPhoto.get(key);
			String url = key;
			urls.add(url);
			owners.add(owner);
			Log.d(ARG, owner+":"+url);
		}
    	i.putExtra("urls", urls.toArray(new String[urls.size()]));
    	i.putExtra("owners", owners.toArray(new String[owners.size()]));
    	
    	startActivity(i);
	}
	
}
