package com.entities;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.functionalities.Functions;

public class FBBatchRequestThread implements Runnable {
	
	private String ID = null;
	private Session session = null;
	Lock lock = new Lock();
	public FBBatchRequestThread(String ID){
		this.ID = ID;
		this.session = Session.getActiveSession();
		
	}
	public String getIDs(){
		return this.ID;
	}
	
	@Override
	public void run() {
		if(ID != null){
				String fqlQuery = "SELECT page_id, pic,pic_cover, description,website,name,location,phone, type,checkins,categories, culinary_team, restaurant_services, restaurant_specialties, food_styles,fan_count, price_range FROM page where page_id IN "
				  		+ "(SELECT page_id FROM place where page_id IN "
				  		+ "(SELECT place FROM stream WHERE (post_id IN "
				  		+ "(SELECT post_id FROM checkin where author_uid = \""+ID+"\" and post_id limit 10000 offset 0) OR "
				  		+ "post_id IN (select post_id from location_post where author_uid = \""+ID+"\" and post_id limit 10000 offset 0)) and like_info.user_likes=1))";
				 
				Bundle param = new Bundle();
				param.putString("q", fqlQuery);
				
				Request request = new Request(session, "/fql", param, HttpMethod.GET, new Request.Callback() {
								@Override
								public void onCompleted(Response response) {
									// TODO Auto-generated method stub
									FacebookRequestError error = response.getError();
									if(error!=null){
										//Log.i("FQL", fqlQuery);
										Log.d("Error", error.getErrorMessage());
									}else{
										GraphObject graphObject = response.getGraphObject();
										if(graphObject !=null){
											JSONObject obj = graphObject.getInnerJSONObject();
											try {
												//Set to global variable.
												handleFBResponse(obj);
											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										}
									}
								}
							});
			request.executeAndWait();
		}else {
			//Get Data From Facebook page
			
			 String args = "picture.type(normal),description,website,name,location,phone,category,category_list,checkins,cover,id";
			 Bundle params = new Bundle();
		     params.putString("fields", args);
		     params.putInt("limit", 10000);
			 Request request = new Request(session, 
			        "me/likes",params, HttpMethod.GET, new Request.Callback() {
					@Override
					public void onCompleted(Response response) {
						// TODO Auto-generated method stub
						FacebookRequestError error = response.getError();
						if(error!=null){
							//Log.i("Error", error.getErrorMessage());
						}else{
							GraphObject graphObject = response.getGraphObject();
							if(graphObject !=null){
								JSONObject obj = graphObject.getInnerJSONObject();
								try {
									//Set to global variable.
									handleFBResponse(obj);
									
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}
			});
			request.executeAndWait();
		}
	}

	private void handleFBResponse(JSONObject obj) throws JSONException{
		JSONArray array = obj.optJSONArray("data");
		if(array!=null){
			ArrayList<Restaurant> values = new ArrayList<Restaurant>();
		    for(int i=0;i<array.length(); i++){
				  Restaurant single = new Restaurant();
				  JSONObject entity = (JSONObject) array.get(i);
				  //Get entities belongs to restaurants
				  if(!isARestaurant(entity)){
					     continue;
				  }
				  //Get data from returned JSON object
				  String phone = entity.optString("phone");
				  String website = entity.optString("website");
				  String title = entity.optString("name");
				  String description = entity.optString("description");
				  int checkin = entity.optInt("checkins");
				  //Get Address
				  JSONObject add_obj = entity.optJSONObject("location");
				  String address = "";
				  String latitude = "";
				  String longitude = "";
				  if(add_obj!=null){
					address = add_obj.optString("country")+" "+add_obj.optString("city")+" "+add_obj.optString("street"); 
					latitude = add_obj.optString("latitude");
				    longitude = add_obj.optString("longitude");
				  }
				  
				  //Get coverUrl
				  JSONObject cover_obj = entity.optJSONObject("pic_cover")==null?entity.optJSONObject("cover"):entity.optJSONObject("pic_cover");
				  String cover_url = "";
				  if(cover_obj!=null){
					  cover_url = cover_obj.optString("source");
				  }
				  
				  //Get imageUrl
				  JSONObject imageUrl_obj = entity.optJSONObject("picture");
				  
				  String imageUrl = "";
				  if(imageUrl_obj!=null){
					  imageUrl = imageUrl_obj.optJSONObject("data").optString("url");
				  }else{
					  imageUrl = entity.optString("pic");
				  }
				  //Get facebook page id
				  String id = entity.optString("page_id")==null?entity.optString("id"):entity.optString("page_id");
				  
				  if(id==null){
					  id="";
					  System.out.println(entity.toString());
				  }
				  
				  //Setter
				  single.setID(id);
				  single.setAddress(address);
				  single.setCheckin(checkin);
				  single.setDescription(description);
				  single.setTEL(phone);
				  single.setTitle(title);
				  single.setWebsite(website);
				  single.setImageUrl(imageUrl);
				  single.setLatitude(latitude);
				  single.setLongitude(longitude);
				  single.setDistance();
				  single.setCoverUrl(cover_url);
				  single.setSource("facebook");
				  // avoid adding duplicate restauraut to Parse.
				  if(!Functions.isExistInParse(title) && !Global.titles.contains(title)){
					  Global.titles.add(title);
					  values.add(single);
					  Functions.saveToParse(single);
				  }
			}
		    Global.restaurants.addAll(values);
		 }
    }
	/*
	 * filter out the facebook pages that is not belong to restaurant
	 */
	private boolean isARestaurant( JSONObject entity){
		// culinary_team, restaurant_services, restaurant_specialties
		
		/*if(entity.optString("culinary_team") != "" && entity.optString("restaurant_services") != "" && entity.optString("restaurant_specialties") != "" && entity.optString("food_styles") != ""){
			//Log.i("culinary_team", entity.optString("name")+":"+entity.optString("culinary_team"));
			//Log.i("restaurant_services", entity.optString("name")+":"+entity.optString("restaurant_services"));
			//Log.i("restaurant_specialties", entity.optString("name")+":"+entity.optString("restaurant_specialties"));
			//Log.i("food_styles", entity.optString("name")+":"+entity.optString("food_styles") );
			return true;
	    }*/
		// using the default system Locale
	    Locale defloc = Locale.getDefault();
	    String category = entity.optString("category").toLowerCase(defloc);
	    
		try {
			if(category.contains("restaurant")||category.contains("cafe")||category.contains("café")||category.contains("beverages")){
		
				return true;
			}
			JSONArray array = entity.optJSONArray("category_list") != null ? entity.optJSONArray("category_list"):entity.optJSONArray("categories");
			if(array==null)
				return false;
			for(int i=0;i<array.length();i++){
				JSONObject obj = (JSONObject) array.get(i);
				String category_list = obj.optString("name").toLowerCase(defloc);
				if(category_list.contains("restaurant")||category_list.contains("food")||category_list.contains("café")||category_list.contains("cafe")||category_list.contains("beverages")){
					return true;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
