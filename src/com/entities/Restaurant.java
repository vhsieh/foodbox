package com.entities;

import java.io.Serializable;
import java.util.ArrayList;

import com.functionalities.Functions;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;





/*
 * Define the basic properties of a particular restaurant
 */
public class Restaurant implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String imageUrl;
	private String title;
	private String address;
	private String TEL;
	private String object_id;
	private String Id;
	private String website;
	private String description;
	private int checkin;
	private String longitude;
	private String latitude;
	private double distance;
	private boolean running;
	private String coverUrl;
	private String source;
	private ArrayList<Comment> comments;

	public Restaurant(){
		this.object_id = "";
		this.imageUrl = "";
		this.title = "";
		this.address = "";
		this.TEL = "";
		this.Id = "";
		this.website = "";
		this.description = "";
		this.checkin = 0;
		this.longitude = "";
		this.latitude = "";
		this.coverUrl = "";
		this.comments = null;
		this.source = "";
	}
	
	public Restaurant(String object_id, String imageUrl, String title, String address, String TEL, String ID, String website, String description,
			int checkin, String longitude, String latitude, String coverUrl, ArrayList<Comment> comments){
		this.object_id = object_id;
		this.imageUrl = imageUrl;
		this.title = title;
		this.address = address;
		this.TEL = TEL;
		this.Id = ID;
		this.website = website;
		this.description = description;
		this.checkin = checkin;
		this.longitude = longitude;
		this.latitude = latitude;
		this.coverUrl = coverUrl;
		this.comments = comments;
		
	}
	public void print(){
		System.out.println("ID:"+this.Id);
		System.out.println("Name:"+this.title);
		System.out.println("address:"+this.address);
	}
	public void setSource(String source){
		this.source = source;
	}
	public String getSource(){return this.source;}
	public void setObject_id(String object_id){
		this.object_id = object_id;
	}
	public String getObject_id(){
		if(this.object_id!="")
			return this.object_id;
		String object_id = "";
    	ParseQuery<ParseObject> query = ParseQuery.getQuery("restaurant");
    	query.whereEqualTo("name", this.title);
    	try {
			ParseObject first = query.getFirst();
			object_id = first.getObjectId();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return object_id;
	}
	public void setComments(ArrayList<Comment> comments){
		this.comments = comments;
	}
	public ArrayList<Comment> getComment(){
		return this.comments;
	}
	
	public void setCoverUrl(String coverUrl){
		this.coverUrl = coverUrl;
	}
	public String getCoverUrl(){
		return this.coverUrl;
	}
	public void setLongitude(String longitude){
		this.longitude = longitude;
	}
	public String getLongitude(){
		return this.longitude;
	}
	
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
	public String getLatitude(){
		return this.latitude;
	}
	
	public void setCheckin(int checkin){
		this.checkin = checkin;
	}
	public int getCheckin(){
		return this.checkin;
	}
	public void setDescription(String description){
		this.description = description;
	}
	public String getDescription(){
		return this.description;
	}

	
	public void setWebsite(String website){
		this.website = website;
	}
	public String getWebsite(){
		return this.website;
	}
	
	public void setID(String id){
		this.Id = id;
	}
	public String getID(){
		return this.Id;
	}
	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}
	
	public String getImageUrl(){
		return this.imageUrl;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	public String getTitle(){
		return this.title;
	}
	
	public void setAddress(String address){
		this.address = address;
	}
	public String getAddress(){
		return this.address;
	}
	
	public void setTEL(String TEL){
		this.TEL = TEL;
	}
	public String getTEL(){
		return this.TEL;
	}
	
	public void setDistance(){
		double latitude = 0.0;
		double longitude = 0.0;
		try{
			latitude = Double.valueOf(this.getLatitude());
			longitude = Double.valueOf(this.getLongitude());
			this.distance = Functions.calculateDistance(Global.latitude, Global.longitude, latitude, longitude);
		}catch(Exception e){
		}
		
	}
	public double getDistance(){
		return this.distance;
	}
	
	public boolean isRunning() {
	        return this.running;
    }
	public void setRuning(boolean running) {
	        this.running = running;
	}
	/*
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel p, int flag) {
		// TODO Auto-generated method stub
		p.writeInt(checkin);
		p.writeString(Id);
		p.writeString(TEL);
		p.writeString(address);
		p.writeString(description);
		p.writeString(imageUrl);
		p.writeString(latitude);
		p.writeString(longitude);
	}
	public Restaurant[] newArray(int size) {
		return new Restaurant[size];
		
	}*/
}
