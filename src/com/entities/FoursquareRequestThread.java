package com.entities;

import android.annotation.SuppressLint;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.functionalities.Functions;

@SuppressLint("DefaultLocale")
public class FoursquareRequestThread implements Runnable{
	String access_token = "";
	String v = "";
	public FoursquareRequestThread(String access_token, String v){
		this.access_token = access_token;
		this.v = v;
	}
	/*
	 * Get all the restaurant checkins that current user liked on the Foursquare
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Log.d("access_token", this.access_token);
			
			URL url 	= new URL("https://api.foursquare.com/v2/checkins/recent?oauth_token="+this.access_token+"&v="+this.v);
			
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet pageGet = new HttpGet(url.toURI());
	
			HttpResponse response = httpClient.execute(pageGet);
			HttpEntity entity = response.getEntity();
			String result = streamToString(entity.getContent());
			
			JSONObject jsonObj 	= (JSONObject) new JSONTokener(result).nextValue();

			JSONArray groups	= (JSONArray) jsonObj.getJSONObject("response").getJSONArray("recent");
			
			int length			= groups.length();
			if (length > 0) {
				for (int i = 0; i < length; i++) {
					JSONObject item = (JSONObject) groups.get(i);
					boolean liked  = item.optBoolean("like");
					if(!liked){continue;}
					Restaurant single = new Restaurant();
					JSONObject venue = item.optJSONObject("venue");
					JSONArray categories = venue.optJSONArray("categories");
					String category = "";
					for(int j=0;j<categories.length();j++){
						JSONObject obj = categories.optJSONObject(j);
						String name = obj.optString("name").toLowerCase(Locale.ENGLISH);
						if(name.contains("tea")||name.contains("restaurant")||name.contains("cafeteria")||name.contains("buffets")||name.contains("burger")||name.contains("café")||name.contains("cafe")||name.contains("breakfast")){
							category = name;
						}
					}
					Log.d("category", category);
					if(category.equalsIgnoreCase(""))//probably not a restaurant;
						continue;
					
					String title = venue.optString("name");
					single.setTitle(title);
					
					String ID = item.optString("id");
					single.setID(ID);
					Log.d("ID", ID);
					
					JSONObject contact = venue.optJSONObject("contact");
					String phone = contact.optString("formattedPhone")!=null?contact.optString("formattedPhone"):contact.optString("phone");
					single.setTEL(phone);
					
					JSONObject location = venue.optJSONObject("location");
					String longitude = location.optString("lng");
					String latitude = location.optString("lat");
					single.setLatitude(latitude);
					single.setLongitude(longitude);
					
					String address = location.optString("country")+location.optString("city")+location.optString("address");
					single.setAddress(address);
					single.setSource("foursquare");
					JSONObject status = venue.optJSONObject("status");
					int checkinsCount = status.optInt("checkinsCount");
					single.setCheckin(checkinsCount);
					
					if(!Functions.isExistInParse(title)){
						  Log.d("not existed", "not existed");
						  Global.restaurants.add(single);
						  Functions.saveToParse(single);
					  }else{
						  Log.d("existed", "existed");
					  }
				}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private String streamToString(InputStream is) throws IOException {
		String str  = "";

		if (is != null) {
			StringBuilder sb = new StringBuilder();
			String line;

			try {
				BufferedReader reader 	= new BufferedReader(new InputStreamReader(is));

				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				reader.close();
			} finally {
				is.close();
			}

			str = sb.toString();
		}

		return str;
	}
	
}
