package com.entities;

import java.io.Serializable;


public class Comment implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String comment;
	private String Title;
	private String icon_url;
	private String user_name;
	public Comment(){
		this.comment = "";
		this.Title="";
		this.icon_url = "";
		user_name = "";
	}
	
	public Comment(String comment, String Title, String icon_url, String user_name){
		this.comment = comment;
		this.Title = Title;
		this.icon_url = icon_url;
		this.user_name = user_name;
	}
	public void setUserName(String user_name){this.user_name = user_name;}
	public String getUserName(){return this.user_name;}
	
	public void setIconUrl(String icon_url){this.icon_url = icon_url;}
	public String getIconUrl(){return this.icon_url;}
	
	public void setRestaurantTitle(String Title){this.Title = Title;}
	public String getRestaurantTitle(){return this.Title;}
	
	
	public void setComment(String comment){this.comment = comment;}
	public String getComment(){return this.comment;}
	
}
