package com.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.custom.RecyclingBitmapDrawable;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.util.LruCache;


public class Global {
	public static ArrayList<String> FBIDs = new ArrayList<String>();
	public static List<Restaurant> restaurants = Collections.synchronizedList(new ArrayList<Restaurant>());
	public static List<String> titles = Collections.synchronizedList(new ArrayList<String>());
	public static boolean downloadingTask = true;
	public static Bitmap facebookIcon = null;
	public static String fbName = null;
	public static String iconUrl = null;
	public static String coverUrl = null;
    public static double latitude = 0.0;
	public static double longitude = 0.0;
	
	final static int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

    // Use 1/8th of the available memory for this memory cache.
    final static int cacheSize = maxMemory / 8;
	public static LruCache<String, BitmapDrawable> mMemoryCache = new LruCache<String, BitmapDrawable>(cacheSize) {
        @Override
        protected int sizeOf(String key, BitmapDrawable bd) {
            // The cache size will be measured in kilobytes rather than
            // number of items.
            return bd.getBitmap().getByteCount() / 1024;
        }
        @Override
        protected void entryRemoved(boolean evicted, String key, BitmapDrawable oldValue, BitmapDrawable newValue){
        	if (RecyclingBitmapDrawable.class.isInstance(oldValue)) {
                // The removed entry is a recycling drawable, so notify it
                // that it has been removed from the memory cache.
        		System.out.println(oldValue+"removed from memory");
                ((RecyclingBitmapDrawable) oldValue).setIsCached(false);
                
            } 
        }
    };
	
    public static void addBitmapToMemoryCache(String url, BitmapDrawable bitmap) {
        if (getBitmapFromMemCache(url) == null) {
            mMemoryCache.put(url, bitmap);
        }
    }

    public static BitmapDrawable getBitmapFromMemCache(String url) {
        return mMemoryCache.get(url);
    }
}
