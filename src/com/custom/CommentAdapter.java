package com.custom;

import java.util.ArrayList;
import java.util.List;

import com.entities.Comment;
import com.functionalities.ImageDownloaderWithCache;
import com.utility.foodbox.R;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentAdapter extends ArrayAdapter<Comment>{
	private  Context context;
	private  ArrayList<Comment> allRes = new ArrayList<Comment>();
	
	public CommentAdapter(Context context, int textViewResourceId, ArrayList<Comment> objects) {
		// TODO Auto-generated constructor stub
		super(context, textViewResourceId, objects);
		this.context = context;  
		for(Comment object:objects){
			this.allRes.add(object);
		}
	}
	static class ViewHolder{
		
		public TextView content;
		public ImageView icon;
		public TextView userName;
		
    }
	@Override
	public int getCount(){
		return this.allRes.size();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		View rowView = null;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = convertView;
		if(rowView==null){
				rowView = inflater.inflate(R.layout.comment, parent, false);
				ViewHolder viewHolder = new ViewHolder();
				viewHolder.userName = (TextView)rowView.findViewById(R.id.userName);
				viewHolder.content = (TextView)rowView.findViewById(R.id.comment);
				viewHolder.icon = (ImageView)rowView.findViewById(R.id.writer_icon);
				rowView.setTag(viewHolder);
		}
			 ViewHolder holder = (ViewHolder) rowView.getTag();
			 Comment comment = this.allRes.get(position);
			 holder.content.setText(comment.getComment());
			 
			 int screen_width = rowView.getWidth();
			 LayoutParams params=holder.content.getLayoutParams();
			 params.width=(int) (screen_width-(int)50* getContext().getResources().getDisplayMetrics().density);
			 
			 holder.userName.setText(comment.getUserName()+":");
			 holder.userName.setTextColor(Color.BLUE);
			 float newWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getContext().getResources().getDisplayMetrics());
			 new ImageDownloaderWithCache(this.context.getApplicationContext(), newWidth).download(comment.getIconUrl(), holder.icon);
		     rowView.setPadding(0, 10, 0, 10);
		return rowView;
		
	}
	public void addItems(List<Comment> list){
		if(allRes!=null){
			for(Comment r:list){
				if(!allRes.contains(r))
					allRes.add(r);
				else
					System.out.println(r.getComment());
			}
			
		}else{
			allRes = (ArrayList<Comment>) list;
		}
	}
	public int size(){
		return this.allRes.size();
	}
	
}
