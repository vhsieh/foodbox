package com.custom;

import java.util.Comparator;

import com.entities.Restaurant;

public class CheckInComparator implements Comparator<Restaurant>{

	@Override
	public int compare(Restaurant lhs, Restaurant rhs) {
		// TODO Auto-generated method stub
		if(lhs.getCheckin() < rhs.getCheckin())
			return 1;
		else if(lhs.getCheckin() > rhs.getCheckin())
			return -1;
		else
			return 0;
	}

}
