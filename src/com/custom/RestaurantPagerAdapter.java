package com.custom;

import java.util.List;

import com.entities.Restaurant;
import com.entities.RestaurantFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

public class RestaurantPagerAdapter extends FragmentStatePagerAdapter{
	List<Restaurant> values = null; 
	private static final String TAG = "RestaurantPagerAdapter";
	public RestaurantPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}
	public RestaurantPagerAdapter(FragmentManager fm, List<Restaurant> values) {
		
		// TODO Auto-generated constructor stub
		super(fm);
		this.values = values;
	}
	@Override
	public Fragment getItem(int position) {
		// TODO Auto-generated method stub
		Log.d(TAG, "getItem called");
		/*Fragment fragment = new RestaurantFragment();
		
        Bundle args = new Bundle();
        // Our object is just a Restaurant
        args.putSerializable("Restaurant", values.get(position));
        fragment.setArguments(args);*/
        return RestaurantFragment.newInstance(position);//fragment;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.values.size();
	}
	@Override
	public CharSequence getPageTitle(int position) {
	    return this.values.get(position).getTitle();
	}
	@Override  
    public void destroyItem(View container, int position, Object object) {  
        ((ViewPager)container).removeViewAt(position);  
    }  
}
