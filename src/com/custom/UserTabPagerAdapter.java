package com.custom;

import com.entities.BasicInfo;
import com.entities.FriendInfo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class UserTabPagerAdapter extends FragmentStatePagerAdapter{

	public UserTabPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		// TODO Auto-generated method stub
		switch (index){
			case 0:
				return new BasicInfo();
			case 1:
				return new FriendInfo();
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2;
	}

}
