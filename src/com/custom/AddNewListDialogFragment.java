package com.custom;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.parse.ParseObject;
import com.parse.ParseUser;
import com.utility.foodbox.R;

public class AddNewListDialogFragment extends DialogFragment{
	EditText title;
	EditText description;
	 /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */

    public interface AddNewListDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }
 // Use this instance of the interface to deliver action events
    AddNewListDialogListener mListener;
    
    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the AddNewListDialogListener so we can send events to the host
            mListener = (AddNewListDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString() + " must implement NoticeDialogListener");
        }
    }
    
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    // Get the layout inflater
	    LayoutInflater inflater = getActivity().getLayoutInflater();
	    View view = inflater.inflate(R.layout.add_new_list_dialog, null);
	    title = (EditText)view.findViewById(R.id.title);
	    description = (EditText)view.findViewById(R.id.description);
	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
	    builder.setView(view)
	    // Add action buttons
	           .setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	            	   
	            	   AddListToParse();
	            	   mListener.onDialogPositiveClick(AddNewListDialogFragment.this);
	            	   Log.d(getTag(), String.valueOf(id));
	               }
	           })
	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	            	   mListener.onDialogNegativeClick(AddNewListDialogFragment.this);
	               }
	           });      
	    return builder.create();
    }
    public void AddListToParse(){
    	
    	//add to to-eat-list table
    	String titleText = title.getText().toString();
    	String descriptionText = description.getText().toString();
    	if(titleText!=null){
    		Log.d(getTag(), "saving");
    		ParseObject obj = new ParseObject("to_eat_list");
        	obj.put("user", ParseUser.getCurrentUser());
        	obj.put("group", titleText);
        	obj.put("description", descriptionText);
        	obj.saveInBackground();
    	}
    	//refer this list to User
    	ParseUser user = ParseUser.getCurrentUser();
    	user.add("to_eat_list", titleText);
    	user.saveInBackground();
    	
    }
}
