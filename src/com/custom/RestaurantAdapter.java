package com.custom;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.entities.Restaurant;
import com.functionalities.DrawableBackgroundDownloader;
import com.utility.foodbox.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RestaurantAdapter extends ArrayAdapter<Restaurant>{
	private  Context context; 
	@SuppressLint("UseSparseArrays")
	private  ArrayList<Restaurant> allRes = new ArrayList<Restaurant>();
	DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();
	
	public RestaurantAdapter(Context context, int textViewResourceId, List<Restaurant> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;  
		for (int i = 0; i < objects.size(); ++i) {
	    	  this.allRes.add(objects.get(i));
	      }
	}
	
	 public void removeDuplicates(){
     	ArrayList<Restaurant> al = this.allRes;
     	ArrayList<Restaurant> al2 = new ArrayList<Restaurant>();
     	Iterator<Restaurant> i = al.iterator();
     	while(i.hasNext()){
     		Restaurant r = (Restaurant)i.next();
     		if(!al2.contains(r))
     			al2.add(r);
     	}
     	this.allRes.clear();
     	this.allRes = al2;
     }
	public static class ViewHolder{
		public String ID;
		public TextView title;
		public ImageView icon;
		public TextView phone;
		public TextView checkins;
		public TextView address;
		public TextView distance;
		public Restaurant r;
		public int position;
		
    }
	
	@SuppressLint("NewApi")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent){
		
		View rowView = convertView;
		if(rowView==null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.row, parent, false);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.title = (TextView)rowView.findViewById(R.id.title);
			viewHolder.icon = (ImageView)rowView.findViewById(R.id.icon);
			viewHolder.phone = (TextView)rowView.findViewById(R.id.phone);
			viewHolder.checkins = (TextView)rowView.findViewById(R.id.checkins);
			viewHolder.address = (TextView)rowView.findViewById(R.id.adress);
			viewHolder.distance = (TextView)rowView.findViewById(R.id.location);
			viewHolder.ID = this.allRes.get(position).getID();
			rowView.setTag(viewHolder);
		}
		 ViewHolder holder = (ViewHolder) rowView.getTag();
		
		 Restaurant restaurant = this.allRes.get(position);
		 holder.title.setText(restaurant.getTitle());
		 //Download image in background
		 if(restaurant.getImageUrl()!=""){
			
			Drawable d = this.context.getResources().getDrawable(R.drawable.download_in_process);
			downloader.loadDrawable(restaurant.getImageUrl(), holder.icon, d);
			
		 }
		 holder.phone.setText(restaurant.getTEL());
		 holder.checkins.setText(String.valueOf(restaurant.getCheckin())+"���d");
		 holder.address.setText(restaurant.getAddress());
		 holder.distance.setText("�Z��"+String.valueOf(restaurant.getDistance())+"km");
		 return rowView;
	}
	public void addItems(List<Restaurant> list){
		if(allRes!=null){
			allRes.addAll(list);
		}else{
			allRes = (ArrayList<Restaurant>) list;
		}
	}
	public void removeItem(int position){
		this.allRes.remove(position);
	}
	
	public ArrayList<Restaurant> getAllItem(){
		return this.allRes;
	}
}
