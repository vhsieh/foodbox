package com.custom;

import java.util.List;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.utility.foodbox.R;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ToEatListAdapter extends ArrayAdapter<String>{
	private Context mContext;
	private List<String> items;
	private String object_id;
	public ToEatListAdapter(Context context, int resource, List<String> items, String title, String object_id) {
		super(context, resource, items);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.items = items;
		this.object_id = object_id;
	}
	public static class ViewHolder{
		public TextView item;
    }
	public View getView(final int position, View convertView, ViewGroup parent){
		View rowView = convertView;
		if(rowView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.single_list, parent, false);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.item = (TextView)rowView.findViewById(R.id.single_list);
			rowView.setTag(viewHolder);
		}
		final ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.item.setText(this.items.get(position));
		holder.item.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				new Thread(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						saveToEatListToParse(items.get(position));
					}
				}).start();
			}
		});
		
		return rowView;
	}
	private void saveToEatListToParse(final String item){
		Log.d("toDoList", "toDoList");
		ParseUser user = ParseUser.getCurrentUser();
		if(item.equals("Create a new one")){
			
		}else{
			try {
				@SuppressWarnings("unchecked")
				List<String> existed_list = (List<String>) ParseUser.getCurrentUser().get("to_eat_list");
				Log.d("toDoList", String.valueOf(existed_list==null));
				for(String list:existed_list){
					if(list==item){
						ParseQuery<ParseObject> query = ParseQuery.getQuery("to_eat_list");
						query.whereEqualTo("group", list);
						query.whereEqualTo("user", user);
						ParseObject obj = query.getFirst();
						obj.add("object_ids", this.object_id);
						obj.saveEventually();
						break;
					}
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
