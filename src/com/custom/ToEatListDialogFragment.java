package com.custom;

import java.util.List;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.utility.foodbox.R;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.ListView;

@SuppressLint("ValidFragment")
public class ToEatListDialogFragment extends DialogFragment {
	private List<String> list;
	@SuppressWarnings("unused")
	private String title;
	private String object_id;
	ListView listview;
	public ToEatListDialogFragment(List<String> list, String title, String object_id){
		this.list = list;
		this.title = title;
		this.object_id = object_id;
	}/*
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
		View content = inflater.inflate(R.layout.to_eat_list_dialog, container, false);
		ListView listview = (ListView) content.findViewById(R.id.items);
		//ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
		ToEatListAdapter adapter = new ToEatListAdapter(getActivity(), android.R.layout.simple_list_item_1, list, title, object_id);
		listview.setAdapter(adapter);
        return content;
    }*/
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    // Get the layout inflater
	    /*LayoutInflater inflater = getActivity().getLayoutInflater();
	    View view = inflater.inflate(R.layout.to_eat_list_dialog, null);
	    listview = (ListView)view.findViewById(R.id.items);
	    ToEatListAdapter adapter = new ToEatListAdapter(getActivity(), android.R.layout.simple_list_item_1, list, title, object_id);
		listview.setAdapter(adapter);*/
		String[] items = new String[list.size()];
		items = list.toArray(items);
	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
	    builder//.setView(view)
		    .setItems(items, new OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Log.d("click", list.get(which));
					saveToEatListToParse(list.get(which));
				}
		    	
		    })
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	            	  // mListener.onDialogNegativeClick(AddNewListDialogFragment.this);
	            	   ToEatListDialogFragment.this.getDialog().cancel();
	               }
	        })
	        // Add action buttons
	        .setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	            	   
	            	   DialogFragment create_dialog = new AddNewListDialogFragment();
	            	   create_dialog.show(getFragmentManager(), getTag());
	               }
	        });
	    return builder.create();
    }
	private void saveToEatListToParse(String item){
		Log.d("toDoList", "toDoList");
		ParseUser user = ParseUser.getCurrentUser();
		try {
			@SuppressWarnings("unchecked")
			List<String> existed_list = (List<String>) ParseUser.getCurrentUser().get("to_eat_list");
			Log.d("toDoList", String.valueOf(existed_list==null));
			for(String list:existed_list){
				if(list==item){
					ParseQuery<ParseObject> query = ParseQuery.getQuery("to_eat_list");
					query.whereEqualTo("group", list);
					query.whereEqualTo("user", user);
					ParseObject obj = query.getFirst();
					obj.addUnique("object_ids", this.object_id);
					obj.saveEventually();
					break;
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
