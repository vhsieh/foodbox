package com.custom;

import java.util.HashMap;

import com.entities.Global;
import com.functionalities.ImageDownloaderWithCache;
import com.utility.foodbox.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class LeftDrawerAdapter extends ArrayAdapter<String>{
	
	private  Context context; 
	@SuppressLint("UseSparseArrays")
	private  HashMap<Integer, String> functionalities = new HashMap<Integer, String>();
	private  ImageView icon;
	private  TextView userNameView;
	public LeftDrawerAdapter(Context context, int textViewResourceId, String[] mPlanetTitles) {
		super(context, textViewResourceId, mPlanetTitles);
		this.context = context;  
		for (int i = 0; i < mPlanetTitles.length; ++i) {
			this.functionalities.put(i, mPlanetTitles[i]);
	    }
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = null;
		if(position == 0){//Set up FB information
			rowView = inflater.inflate(R.layout.fb_user_icon, parent, false);
			icon = (ImageView)rowView.findViewById(R.id.userProfilePicture);
			userNameView = (TextView)rowView.findViewById(R.id.userName);
			
			if(Global.facebookIcon!=null)
				icon.setImageBitmap(Global.facebookIcon);
			else{
				float targetWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, this.getContext().getResources().getDisplayMetrics());
				float targetHeight = targetWidth;
				
				new ImageDownloaderWithCache(context.getApplicationContext(), true, targetWidth, targetHeight).download(Global.iconUrl, icon);
				//new ImageDownloader(icon, targetWidth, targetHeight, true).execute(Global.iconUrl);
			}
			userNameView.setText(Global.fbName);
		}else{
			rowView= inflater.inflate(R.layout.left_drawer, parent, false);
			//ImageView mImageView = (ImageView)rowView.findViewById(R.id.function_icon);
			TextView action = (TextView)rowView.findViewById(R.id.functionality);
			action.setText(this.functionalities.get(position));
			Resources res = getContext().getResources();// need this to fetch the drawable
			Drawable draw = null;
			switch(position){
		
			    case 1:
			    	draw = res.getDrawable(R.drawable.home);
					//mImageView.setImageDrawable(draw);
					break;
			    case 2:
			    	draw = res.getDrawable(R.drawable.to_eat);
			    	//mImageView.setImageDrawable(draw);
			    	break;
			    case 3:
			    	draw = res.getDrawable(R.drawable.near_by);
			    	//mImageView.setImageDrawable(draw);
			    	break;
			    case 4:
			    	draw = res.getDrawable(R.drawable.setting);
			    	//mImageView.setImageDrawable(draw);
			    	break;
			    	
			}
			action.setCompoundDrawablesWithIntrinsicBounds(draw, null, null, null);
			
		}

		return rowView;
	}
	/*
	
	private void onLogoutButtonClicked() {
		// Log the user out
		ParseUser.logOut();

		// Go to the login view
		startLoginActivity();
	}
	
	private void startLoginActivity() {
		Intent intent = new Intent(context, Login.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}*/
	
}
