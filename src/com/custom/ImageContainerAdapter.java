package com.custom;

import com.functionalities.DrawableBackgroundDownloader;
import com.utility.foodbox.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageContainerAdapter extends ArrayAdapter<String>{
	private  Context context; 
	private  String[] urls;
	private String[] owners;
	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();
	public ImageContainerAdapter(Context context, int textViewResourceId, String[] urls, String[] owners) {
		super(context, textViewResourceId, urls);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.urls = urls;
		this.owners = owners;
	}
	public static class ViewHolder{

		public ImageView image;
		public TextView num;
		public TextView owner;
		
    }
	@Override
	public View getView(final int position, View convertView, ViewGroup parent){
		View rowView = convertView;
		if(rowView==null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.single_image, parent, false);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.image = (ImageView)rowView.findViewById(R.id.single_image);
			viewHolder.num = (TextView)rowView.findViewById(R.id.image_number);
			viewHolder.owner = (TextView)rowView.findViewById(R.id.owner);
			rowView.setTag(viewHolder);
		}
		ViewHolder holder = (ViewHolder) rowView.getTag();
		String url = this.urls[position];
		//BitmapDrawable image = Global.getBitmapFromMemCache(url);
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		@SuppressWarnings("deprecation")
		int width = display.getWidth();
		@SuppressWarnings("deprecation")
		int height = display.getHeight();
		
		Drawable d = this.context.getResources().getDrawable(R.drawable.download_in_process);
    	downloader.loadDrawable(url, holder.image, d);
    	
		holder.num.setText(String.valueOf(position+1)+"/"+this.urls.length);
		holder.owner.setText(this.owners[position]);
		LayoutParams params = (LayoutParams) rowView.getLayoutParams();
		params.height = height;
		params.width = width;
		rowView.setLayoutParams(params);
		
		return rowView;
		
	}
}
