package com.custom;

import java.util.Comparator;

import com.entities.Restaurant;

public class DistanceComparator implements Comparator<Restaurant>{

	@Override
	public int compare(Restaurant lhs, Restaurant rhs) {
		// TODO Auto-generated method stub
		if(lhs.getDistance() > rhs.getDistance())
			return 1;
		else if(lhs.getDistance() < rhs.getDistance())
			return -1;
		else
			return 0;
	}
	
}
