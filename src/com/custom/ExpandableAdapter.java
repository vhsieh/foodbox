package com.custom;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import com.functionalities.DrawableBackgroundDownloader;
import com.utility.foodbox.R;

public class ExpandableAdapter extends BaseExpandableListAdapter
{
		private Context context;
		private JSONArray to_eat_list = new JSONArray();
		DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();
		public ExpandableAdapter(Context context, JSONArray to_eat_list)
		{
			this.to_eat_list = to_eat_list;
			this.context = context;
			
		}
		public static class ViewHolderChild{
			public TextView child_title;
			public ImageView child_icon;
	    }
		public static class ViewHolderGroup{
			public TextView group;
		}
		@Override
		public JSONObject getChild(int groupPosition, int childPosition)
		{
			return to_eat_list.optJSONObject(groupPosition).optJSONArray("childs").optJSONObject(childPosition);//childs.get(groupPosition).get(childPosition);
		}
		@Override
		public long getChildId(int groupPosition, int childPosition)
		{
			return childPosition;
		}

		public int getChildrenCount(int groupPosition)
		{
			return to_eat_list.optJSONObject(groupPosition).optJSONArray("childs").length();//childs.get(groupPosition).size();
		}

		public JSONObject getGroup(int groupPosition)
		{
			return to_eat_list.optJSONObject(groupPosition);//groups.get(groupPosition);
		}

		public int getGroupCount()
		{
			return to_eat_list.length();
		}

		public long getGroupId(int groupPosition)
		{
			return groupPosition;
		}

		public boolean hasStableIds()
		{
			return false;
		}
		
		public boolean isChildSelectable(int groupPosition, int childPosition)
		{
			return false;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View rowView = convertView;
			if(rowView==null){
				LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.child, parent, false);
				ViewHolderChild view = new ViewHolderChild();
				view.child_icon = (ImageView)rowView.findViewById(R.id.child_icon);
				view.child_title = (TextView)rowView.findViewById(R.id.child_title);
				rowView.setTag(view);
			}
			ViewHolderChild holder = (ViewHolderChild) rowView.getTag();
			String title = getChild(groupPosition, childPosition).optString("name");
			String image_url = getChild(groupPosition, childPosition).optString("iconUrl");
			Log.d("url", image_url);
			Drawable d = this.context.getResources().getDrawable(R.drawable.download_in_process);
			downloader.loadDrawable(image_url, holder.child_icon, d);
			holder.child_title.setText(title);
			return rowView;
		}
		
		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View rowView = convertView;
			if(rowView==null){
				LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.group, parent, false);
				ViewHolderGroup view = new ViewHolderGroup();
				view.group = (TextView)rowView.findViewById(R.id.group);
				rowView.setTag(view);
			}
			ViewHolderGroup holder = (ViewHolderGroup) rowView.getTag();
			String title = getGroup(groupPosition).optString("group");
			holder.group.setText(title);
			return rowView;
		}

}
