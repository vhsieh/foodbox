package com.custom;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.utility.foodbox.R;

public class OptionsAdapter extends ArrayAdapter<String> {
	private String[] options;
	private Context mContext;
	public OptionsAdapter(Context context, int resource, String[] options) {
		super(context, resource, options);
		this.options = options;
		this.mContext = context;
		// TODO Auto-generated constructor stub
	}
	
	@Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt);
    }
    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }
    public View getCustomView(int position, View convertView, ViewGroup parent) {
    	LayoutInflater inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mySpinner = inflater.inflate(R.layout.option_spinner, parent, false);
        TextView main_text = (TextView) mySpinner
                .findViewById(R.id.option_text);
        main_text.setText(this.options[position]);
        main_text.setGravity(Gravity.CENTER);
        return mySpinner;
    }
}
