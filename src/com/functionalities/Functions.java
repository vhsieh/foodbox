package com.functionalities;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import com.entities.Restaurant;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;



@SuppressLint("SimpleDateFormat")
public class Functions {
	public final static double AVERAGE_RADIUS_OF_EARTH = 6371;

	
	public static int calculateDistance(double userLat, double userLng, double venueLat, double venueLng) {

	    double latDistance = Math.toRadians(userLat - venueLat);
	    double lngDistance = Math.toRadians(userLng - venueLng);

	    double a = (Math.sin(latDistance / 2) * Math.sin(latDistance / 2)) +
	                    (Math.cos(Math.toRadians(userLat))) *
	                    (Math.cos(Math.toRadians(venueLat))) *
	                    (Math.sin(lngDistance / 2)) *
	                    (Math.sin(lngDistance / 2));

	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

	    return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH * c));
	}
	
	
	public static void saveToParse(Restaurant r){
		
        ParseObject single = new ParseObject("restaurant");
    	single.put("id", r.getID());
    	single.put("source", r.getSource());
    	single.addUnique("user", ParseUser.getCurrentUser());
    	single.put("name", r.getTitle());
    	single.put("phone", r.getTEL());
    	single.put("address", r.getAddress());
    	single.put("website", r.getWebsite());
    	single.put("description", r.getDescription());
    	single.put("checkin", r.getCheckin());
    	//create a GeoPoint 
    	if(r.getLongitude()!="" && r.getLatitude()!=""){
    		ParseGeoPoint point = new ParseGeoPoint(Double.valueOf(r.getLatitude()), Double.valueOf(r.getLongitude()));
    		single.put("point", point);
    	}
    	single.put("imageUrl", r.getImageUrl());
    	single.put("coverUrl", r.getCoverUrl());
    	single.saveEventually();
    }
	/*
	 * Check if a restaurant is already stored in Parse.
	 */
	public synchronized static boolean isExistInParse(String title){
    	ParseQuery<ParseObject> query = ParseQuery.getQuery("restaurant");
    	query.whereEqualTo("name", title); 
    	ParseObject obj = null;
		try {
			obj = query.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
		}
    	if(obj!=null){//exist in Parse
    		obj.addUnique("user", ParseUser.getCurrentUser());
    		obj.saveEventually();
    		return true;
    	}
    	return false;
    }

	public static String GetAddressByLatLon(Context mContext, String longitude, String latitude) {
		Geocoder gc = new Geocoder(mContext, Locale.TRADITIONAL_CHINESE);
		String returnAddress = "";
		try {
			List<Address> lstAddress = gc.getFromLocation(Double.valueOf(latitude), Double.valueOf(longitude), 1);
			returnAddress =lstAddress.get(0).getAddressLine(0);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnAddress;
	}
	public static HashMap<String, String> GetLatLngByAddress(Context mContext, String address){
		HashMap<String, String> LatLng = new HashMap<String, String>();
		Geocoder gc = new Geocoder(mContext, Locale.TRADITIONAL_CHINESE);
		try {
			List<Address> lstAddress = gc.getFromLocationName(address, 1);
			Double longitude = lstAddress.get(0).getLongitude();
			Double latitude = lstAddress.get(0).getLatitude();
			LatLng.put("longitude", String.valueOf(longitude));
			LatLng.put("latitude", String.valueOf(latitude));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return LatLng;
	}
	public static String timeMilisToString(long milis) {
		SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar   = Calendar.getInstance();

		calendar.setTimeInMillis(milis);

		return sd.format(calendar.getTime());
	}
}
