package com.functionalities;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.custom.AddNewListDialogFragment;
import com.custom.ExpandableAdapter;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.utility.foodbox.R;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.Map;
public class ToEatList extends Fragment implements AddNewListDialogFragment.AddNewListDialogListener{
	public static final String ARG = "ToEatList";
	private View rootView;
	ExpandableAdapter adapter;
	ExpandableListView elv;
	private JSONArray to_eat_list = new JSONArray();
	Handler mHandler = new Handler(Looper.getMainLooper());
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
	}
	@Override
    public void onCreate(Bundle savedInstanceState){
    	super.onCreate(savedInstanceState);
    	
    }
	
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	setHasOptionsMenu(true);
		if(rootView==null){
			rootView = inflater.inflate(R.layout.to_eat_list, container, false);
			elv = (ExpandableListView)rootView.findViewById(R.id.mExpandableListView);
			//
		}else{
        	ViewGroup parent = (ViewGroup) rootView.getParent();
            parent.removeView(rootView);
        }
    	return rootView;
    }
    @Override  
    public void onActivityCreated(Bundle savedInstanceState) {  
        super.onActivityCreated(savedInstanceState);  
        Log.d(ARG, "onActivityCreated");
        
    } 
    @Override  
    public void onStart() {  
        super.onStart();  
        
        Log.d(ARG, "onStart");  
    }  
    @Override
    public void onResume(){
    	super.onResume();
    	adapter = new ExpandableAdapter(this.getActivity().getApplicationContext(), to_eat_list);
		elv.setAdapter(adapter);
    	new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				FetchToEatList();
			}
    		
    	}).start();
        
    	Log.d(ARG, "onResume");
    }
    @Override  
    public void onPause() {  
        super.onPause();  
        Log.d(ARG, "onPause");  
    }  
  
    @Override  
    public void onStop() {  
        super.onStop();  
        Log.d(ARG, "onStop");  
    }  
  
    @Override  
    public void onDestroyView() {  
        super.onDestroyView();  
        Log.d(ARG, "onDestroyView");  
    }  
  
    @Override  
    public void onDestroy() {  
        super.onDestroy();  
        Log.d(ARG, "onDestroy");  
    }  
  
    @Override  
    public void onDetach() {  
        super.onDetach();  
        Log.d(ARG, "onDetach");  
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    	getActivity().getMenuInflater().inflate(R.menu.add_to_eat_list, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
		if (itemId == R.id.action_new_event) {
			createNewToEatList();
			return true;
		} else if (itemId == R.id.action_settings) {
			//openSettings();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
    }
    @SuppressWarnings("unchecked")
	private void FetchToEatList(){
    	/*
    	 * start loading effect 
    	 */
    	mHandler.post(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
		    	getActivity().setProgressBarIndeterminateVisibility(true);
			}});
    	List<String> existed_list = (List<String>) ParseUser.getCurrentUser().get("to_eat_list");
    	Log.d(ARG, existed_list.toString());
    	Map<String, String> tmp_group = new HashMap<String, String>();
    	if(existed_list!=null){
    		for(String group:existed_list){
    			ParseQuery<ParseObject> query = ParseQuery.getQuery("to_eat_list");
    			query.whereEqualTo("group", group);
    			query.whereEqualTo("user", ParseUser.getCurrentUser());
    			tmp_group.put("group", group);
    			try {
					ParseObject obj = query.getFirst();
					List<String> object_ids = (List<String>) obj.get("object_ids");
					if(object_ids!=null)
						previewRestaurant(group, object_ids);
					else{
						JSONObject single_group = new JSONObject();
						single_group.put("group", group);
						single_group.put("childs", new JSONArray());
						to_eat_list.put(single_group);
						//this.adapter.notifyDataSetChanged();
						
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		mHandler.post(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					getActivity().setProgressBarIndeterminateVisibility(false);
					adapter.notifyDataSetChanged();
				}
    			
    		});
    		
    	}
    }
    private void previewRestaurant(String group_name, List<String> object_ids){
    	try{
	    	JSONObject group = new JSONObject();
	    	JSONArray array_of_restaurant = new JSONArray();
	    	for(String object_id:object_ids){
	    		ParseQuery<ParseObject> query = ParseQuery.getQuery("restaurant");
	    		query.whereEqualTo("objectId", object_id);
	    		ParseObject obj = query.getFirst();
				String title = obj.getString("name");
				String iconUrl = obj.getString("imageUrl");
				JSONObject restaurant = new JSONObject();
				restaurant.put("name", title);
				restaurant.put("iconUrl", iconUrl);
				array_of_restaurant.put(restaurant);
			}
	    	group.put("group", group_name);
	    	group.put("childs", array_of_restaurant);
	    	to_eat_list.put(group);
	    	
    	}catch(JSONException e1){
    		e1.printStackTrace();
    	}catch(ParseException e2){
    		e2.printStackTrace();
    	}
    }
    /*
     * Action to create a new to-eat-list
     */
    private void createNewToEatList(){
    	DialogFragment dialog = new AddNewListDialogFragment();
    	dialog.show(getFragmentManager(), ARG);
    }
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
}
