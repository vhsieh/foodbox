package com.functionalities;



import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import com.custom.DistanceComparator;
import com.custom.RestaurantAdapter;
import com.entities.Comment;
import com.entities.CustomThreadPoolExecutor;
import com.entities.FBBatchRequestThread;
import com.entities.FoursquareRequestThread;
import com.entities.Global;
import com.entities.Restaurant;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.utility.foodbox.HomeFragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

public class home extends ListFragment{
	
	public static final String ARG = "home";
	private RestaurantAdapter adapter = null;
	
	private int poolSize = 5;
	
	private int maxPoolSize = 15;
 
	private long keepAliveTime = 1;
    
	private ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(3);
    final CustomThreadPoolExecutor ctpe = new CustomThreadPoolExecutor(poolSize, maxPoolSize,
            keepAliveTime, TimeUnit.SECONDS, queue);
    ProgressDialog pg = null;
    Handler mHandler = new Handler(Looper.getMainLooper());
    private int mProgressStatus;
    private int current = 0;
    private int currentDisplayItems = 0;
    private ArrayList<Restaurant> values = new ArrayList<Restaurant>();
    @Override
    public void onCreate(Bundle savedInstanceState){
    	super.onCreate(savedInstanceState);
    	getActivity().setProgressBarIndeterminateVisibility(true);
    	adapter = new RestaurantAdapter(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, values);
    	setListAdapter(adapter);
    	
    }

    @Override
	public void onResume(){
		Log.d(ARG, "onResume called");
		super.onResume();
		//we fetch to-eat-list from Parse here
	}
    
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		    super.onActivityCreated(savedInstanceState);
	    	
	    	// create a new thread to check data 
	    	new Thread(new Runnable(){
				@Override
				public void run() {
					// TODO Auto-generated method stub
					fetchDataFromParse();
					if(Global.restaurants.size() == 0){
						mHandler.post(new Runnable(){// we first start a progressdialog to display the progress of the download task
							@Override
							public void run() {
								// TODO Auto-generated method stub
								getActivity().setProgressBarIndeterminateVisibility(false);
								openProgressBar();
							}
						});
						setRestaurants();
					}else{// some data in stored in Parse, fetch them and put them into listview
						mHandler.post(new Runnable(){

							@Override
							public void run() {
								// TODO Auto-generated method stub
								getActivity().setProgressBarIndeterminateVisibility(false);
								displayItems(0);
							}
						});
					}
				}
	    	}).start();
	  }
	  
	  public void openProgressBar(){
		    //Set up progress bar
		    pg = new ProgressDialog(this.getActivity());
	    	pg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	    	pg.setMax(100);
	    	pg.setProgress(0);
	    	pg.setTitle("Download");
	    	pg.setMessage("Please wait..");
	    	pg.setCancelable(false);
	    	pg.show();
	    	
		    new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(mProgressStatus <100){
					mProgressStatus = updateProgressBar();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					mHandler.post(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							pg.setProgress(mProgressStatus);
							//values = Global.restaurants;
						}
					});
		        }
			    if(mProgressStatus >=100){
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						pg.dismiss();
						mHandler.post(new Runnable(){

							@Override
							public void run() {
								// TODO Auto-generated method stub
								displayItems(0);
							}
						});
			    } 
			}
			  
		  }).start();
	   }
	  /*
	   * Sending request to Facebook.
	   */
	  public void setRestaurants(){
		    ctpe.setRejectedExecutionHandler(new RejectedExecutionHandler(){
				@Override
				public void rejectedExecution(Runnable r, ThreadPoolExecutor tpe) {
					// TODO Auto-generated method stub
					try{
						Thread.sleep(2000);
					}catch(InterruptedException e){
						e.printStackTrace();
					}
					
					ctpe.submit(r);
				}
		    });
		    
			ctpe.prestartAllCoreThreads();
			//Get Restaurant From "News Feeds" of current loged in Facebook User
			FBBatchRequestThread fbBatchRequestThread;
			for(int i=0;i< Global.FBIDs.size();i++){
				fbBatchRequestThread = new FBBatchRequestThread(Global.FBIDs.get(i));
				ctpe.submit(fbBatchRequestThread);
			}
			//Now Get Restaurants From the Fans Page of Current Login FB User likes
			FBBatchRequestThread fbRequest = new FBBatchRequestThread(null);
			ctpe.submit(fbRequest);
			
			SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("fqAccessToken", 0);
        	
        	String access_token = sharedPreferences.getString("access_token", "");
        	Log.d(ARG, access_token);
        	String v = Functions.timeMilisToString(System.currentTimeMillis());
        	
			FoursquareRequestThread fqRequest;
			//now we submit foursquare jobs
			fqRequest = new FoursquareRequestThread(access_token, v);
			ctpe.submit(fqRequest);
			// Do not allow adding more task.
			ctpe.shutdown();
			
		}
	    
	    private int updateProgressBar(){
	    	//check compeletion
			Integer numOfTasks = Integer.valueOf((int)Global.FBIDs.size());
			
			int currentProgress = mProgressStatus;
			while(!ctpe.isTerminated()){
				Integer numOfCompleted = Integer.valueOf((int)ctpe.getCompletedTaskCount());
				
				if(numOfCompleted > current){//update the progress status
					current = numOfCompleted;
					currentProgress = Integer.valueOf((int) (numOfCompleted/Double.valueOf(numOfTasks)*100));
					break;
				}
			}
			if(!ctpe.isTerminated())
				if(pg.getProgress()> currentProgress)
					return pg.getProgress();
				else
					return currentProgress;
			else{
				return 100;
			}
	    }
        @Override
	    public void onListItemClick(ListView l, View v, int position, long id) {
        	super.onListItemClick( l, v, position, id);
        	Intent i = new Intent(this.getActivity(), HomeFragment.class);
        	//i.putExtra("restaurant", this.values);
        	i.putExtra("current", position);
        	
	    	startActivity(i);
	    	
		}
        /*
         * Fetch data from Parse.
         */
        private void fetchDataFromParse(){
        	// first empty the stored restaurant
        	Global.restaurants.clear();
	    	//Get stored restaurant by current
	    	ParseQuery<ParseObject> query = ParseQuery.getQuery("restaurant");
	    	query.whereEqualTo("user", ParseUser.getCurrentUser());
	    	try {
				List<ParseObject> data = query.find();
				for(ParseObject p:data){
					String object_id = p.getObjectId();
					String ID = p.getString("id");
					String name = p.getString("name");
					String phone = p.getString("phone");
					String address = p.getString("address");
					String website = p.getString("website");
					String description = p.getString("description");
					int checkin = p.getInt("checkin");
					ParseGeoPoint point = p.getParseGeoPoint("point");
					String longitude = "";
					String latitude = "";
					if(point!=null){
						longitude = String.valueOf(point.getLongitude());
						latitude = String.valueOf(point.getLatitude());
					}
					if(address =="" && (longitude!="" && latitude!="")){
						address = Functions.GetAddressByLatLon(this.getActivity().getApplicationContext(), longitude, latitude);
					}
					if( (longitude=="" && latitude=="") && address!=""){
						HashMap<String, String> LngLat = Functions.GetLatLngByAddress(this.getActivity().getApplicationContext(), address);
						longitude = LngLat.get("longtitude");
						latitude = LngLat.get("latitude");
					}
					String imageUrl = p.getString("imageUrl");
					String coverUrl = p.getString("coverUrl");
					
					//restore comments about this restaurant 
					List<ParseObject> comment_object = new ArrayList<ParseObject>();
					comment_object = p.getList("comments");
					
					ArrayList<Comment> comment = new ArrayList<Comment>();
					if(comment_object!=null){
						for(ParseObject po:comment_object){
							String userName = Global.fbName;
							Comment c = new Comment(po.fetchIfNeeded().getString("comment"), name, po.fetchIfNeeded().getString("icon_url"), userName);
							comment.add(c);
						}
					}
					Restaurant r = new Restaurant(object_id, imageUrl, name, address, phone, ID, website, description, checkin, longitude, latitude, coverUrl, comment);
					
					r.setDistance();
					if(!Global.restaurants.contains(r))
						Global.restaurants.add(r);
				}
				
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	
        }
        /*
         * display the items if size of items excess 20, we increamentally display them.
         */
        public void displayItems(int option){
        	/*switch(option){
        		case 0:
        			Collections.sort(Global.restaurants, new DistanceComparator());
        			break;
        		case 1:
        			Collections.sort(Global.restaurants, new CheckInComparator());
        			break;
        	}*/
        	// default sort by distance of current position
        	Collections.sort(Global.restaurants, new DistanceComparator());
        	
			if(Global.restaurants.size()>20){// if number of items displayed excess 20, we load first 20 items first.
				values.addAll(Global.restaurants.subList(currentDisplayItems, currentDisplayItems+20));
				adapter.addItems(values);
				adapter.notifyDataSetChanged();
				currentDisplayItems+=20;
				
				this.getListView().setOnScrollListener(new OnScrollListener(){

					@Override
					public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
						// TODO Auto-generated method stub
					}
					@Override
					public void onScrollStateChanged(AbsListView view, int scrollState) {
						// TODO Auto-generated method stub
						if(getListView().getLastVisiblePosition() == adapter.getCount()-1){// scroll to the end
							Log.d(ARG, "end");
							int end = currentDisplayItems+20 >= Global.restaurants.size()? Global.restaurants.size()-1:currentDisplayItems+20;
							List<Restaurant> dataToUpdate = Global.restaurants.subList(currentDisplayItems, end);
					        currentDisplayItems = end;
					        if(dataToUpdate!=null && values!=null){
					        	values.addAll(dataToUpdate);
								adapter.addItems(dataToUpdate);
								adapter.notifyDataSetChanged();
					        }
						}
					}
				});
			}else{
				values.addAll(Global.restaurants);
				currentDisplayItems+=Global.restaurants.size();
				adapter.addItems(values);
				//adapter.sort(new DistanceComparator());
				adapter.notifyDataSetChanged();
			}
        }
        @Override
        public void onPause()
        {
          super.onPause();
          System.out.println("onPause called");
          System.gc();
        }
        @Override
        public void onStop(){
        	super.onStop();
        	Global.mMemoryCache.evictAll();
        	values = null;
        }
        @Override
        public void onDestroy()
        {
          super.onDestroy();
          System.gc();
          values = null;
        }
}
