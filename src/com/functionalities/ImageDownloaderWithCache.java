package com.functionalities;

import java.io.InputStream;
import java.lang.ref.WeakReference;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import com.custom.RecyclingBitmapDrawable;
import com.entities.Global;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

public class ImageDownloaderWithCache {
	private boolean isRounded;
	private boolean isResizeHeight;
	private float newWidth;
	private float newHeight;
	private Context mContext;
	
	
	public ImageDownloaderWithCache(Context mContext, float newWidth, float newHeight){
		this.isRounded = false;
		this.newHeight = newHeight;
		this.newWidth = newWidth;
		isResizeHeight = false;
		this.mContext = mContext;
	}
	public ImageDownloaderWithCache(Context mContext, float newWidth){
		this.isResizeHeight = true;
		this.newWidth = newWidth;
		this.mContext = mContext;
	}
	public ImageDownloaderWithCache(Context mContext, boolean isRounded, float newWidth, float newHeight){
		this.isRounded = isRounded;
		this.newHeight = newHeight;
		this.newWidth = newWidth;
		this.mContext = mContext;
	}
	public void download(String url, ImageView imageView) {
		if (cancelPotentialDownload(url, imageView)) {
			
	         BitmapDownloaderTask task = null;
	         if(isRounded)
	        	 task = new BitmapDownloaderTask(mContext, imageView, this.newWidth, this.newHeight, true);
	         else if(isResizeHeight)
	        	 task = new BitmapDownloaderTask(mContext, imageView, this.newWidth);
	         else
	        	 task = new BitmapDownloaderTask(mContext, imageView, this.newWidth, this.newHeight);
	         
	         DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
	         imageView.setImageDrawable(downloadedDrawable);
	         task.execute(url);
	     }
    }
	
	public Bitmap downloadBitmap(String url) {
	    final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
	    final HttpGet getRequest = new HttpGet(url);
	    try {
	        HttpResponse response = client.execute(getRequest);
	        final int statusCode = response.getStatusLine().getStatusCode();
	        if (statusCode != HttpStatus.SC_OK) { 
	            Log.w("ImageDownloader", "Error " + statusCode + " while retrieving bitmap from " + url); 
	            return null;
	        }
	        
	        final HttpEntity entity = response.getEntity();
	        if (entity != null) {
	            InputStream inputStream = null;
	            try {
	                inputStream = entity.getContent(); 
	                BitmapFactory.Options options = new BitmapFactory.Options();
	                /*options.inJustDecodeBounds = true; // first we do not decode the bitmap directly so that avoid OOM
	                BitmapFactory.decodeStream(inputStream, null, options);
	                
	                int sampleSize = calculateInSampleSize(options, (int)(this.newWidth), (int)(this.newHeight));
	                options = new BitmapFactory.Options();
	                Log.d("sample size", String.valueOf(sampleSize));
	                Log.d("inputStream", inputStream.toString());*/
	                //options.inSampleSize = 2;
	                options.inPreferredConfig = Bitmap.Config.ARGB_4444; 
	                options.inPurgeable = true;
	                options.inInputShareable = true;
	                Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
	                return bitmap;
	            } finally {
	                if (inputStream != null) {
	                    inputStream.close();  
	                }
	                entity.consumeContent();
	            }
	        }
	    } catch (Exception e) {
	        // Could provide a more explicit error message for IOException or IllegalStateException
	        getRequest.abort();
	        Log.w("ImageDownloader", "Error while retrieving bitmap from " + url);
	    } finally {
	        if (client != null) {
	            client.close();
	        }
	    }
	    return null;
	}
	/*
	 * caculate the sample size of bitmap
	 */
	public int calculateInSampleSize( BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }

    	return inSampleSize;
	}
	private static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
	    if (imageView != null) {
	        Drawable drawable = imageView.getDrawable();
	        if (drawable instanceof DownloadedDrawable) {
	            DownloadedDrawable downloadedDrawable = (DownloadedDrawable)drawable;
	            return downloadedDrawable.getBitmapDownloaderTask();
	        }
	    }
	    return null;
	}
/*
 * The cancelPotentialDownload method will stop the possible download in progress on this imageView since a new one is about to start. 
 */
	private static boolean cancelPotentialDownload(String url, ImageView imageView) {
	    BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

	    if (bitmapDownloaderTask != null) {
	        String bitmapUrl = bitmapDownloaderTask.url;
	        if ((bitmapUrl == null) || (!bitmapUrl.equals(url))) {
	            bitmapDownloaderTask.cancel(true);
	        } else {
	            // The same URL is already being downloaded.
	            return false;
	        }
	    }
	    return true;
	}
	class BitmapDownloaderTask extends AsyncTask<String, Void, BitmapDrawable> {
	    private String url;
	    private final WeakReference<ImageView> imageViewReference;
	    private boolean isRounded;
		private float newWidth;
		private float newHeight;
	    private boolean isResizeHeight;
	    private Context mContext;
		public BitmapDownloaderTask(Context mContext, ImageView imageView, float newWidth){
			imageViewReference = new WeakReference<ImageView>(imageView);
			this.isResizeHeight = true;
			this.newWidth = newWidth;
			this.mContext = mContext;
		}
		
	    public BitmapDownloaderTask(Context mContext, ImageView imageView, float newWidth, float newHeight) {
	        imageViewReference = new WeakReference<ImageView>(imageView);
	        this.isRounded = false;
	        this.newHeight = newHeight;
	        this.newWidth = newWidth;
	        this.isResizeHeight = false;
	        this.mContext = mContext;
	    }
	    public BitmapDownloaderTask(Context mContext, ImageView imageView,float newWidth,float newHeight,  boolean isRounded){
	    	 imageViewReference = new WeakReference<ImageView>(imageView);
		     this.isRounded = true;
		     this.newHeight = newHeight;
		     this.newWidth = newWidth;
		     this.isResizeHeight = false;
		     this.mContext = mContext;
	    }

	    @Override
	    // Actual download method, run in the task thread
	    protected BitmapDrawable doInBackground(String... params) {
	         // params comes from the execute() call: params[0] is the url.
	    	 Bitmap bitmap = downloadBitmap(params[0]);
	    	 if(this.isRounded){
	    		 bitmap = getRoundedShape(bitmap, this.newWidth, this.newHeight);
	    		 Global.facebookIcon = bitmap;
	    	 }
	    	 else if(this.isResizeHeight){
	    		 float newHeight = getCorrespondenceHeight(bitmap, this.newWidth);
	    		 bitmap = getResizedBitmap(bitmap, newWidth, newHeight);
	    	 }
	    	 RecyclingBitmapDrawable rbd = new RecyclingBitmapDrawable(mContext.getResources(), bitmap);
	    	 Global.addBitmapToMemoryCache(params[0], rbd);
	    	 rbd.setIsCached(true);
	         return rbd;
	    }
	    
	    @Override
	    // Once the image is downloaded, associates it to the imageView
	    protected void onPostExecute(BitmapDrawable bitmap) {
	        if (isCancelled()) {
	            bitmap = null;
	        }

	        if (imageViewReference != null) {
	            ImageView imageView = imageViewReference.get();
	            BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
	            // Change bitmap only if this process is still associated with it
	            if (this == bitmapDownloaderTask) {
	                imageView.setImageDrawable(bitmap);
	            }
	        }
	    }
	}
	
	static class DownloadedDrawable extends ColorDrawable {
	    private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

	    public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask) {
	    	
	        super(Color.GRAY);
	        bitmapDownloaderTaskReference = new WeakReference<BitmapDownloaderTask>(bitmapDownloaderTask);
	    }

	    public BitmapDownloaderTask getBitmapDownloaderTask() {
	        return bitmapDownloaderTaskReference.get();
	    }
	}
	public Bitmap getResizedBitmap(Bitmap bm, float newWidth, float newHeight) {
	    //newWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, getContext().getResources().getDisplayMetrics());
		//newHeight = newWidth;
		
	    int width = bm.getWidth();
		 
		int height = bm.getHeight();
		
		float scaleWidth = (newWidth) / width;
		 
		float scaleHeight = (newHeight) / height;
		 
		// CREATE A MATRIX FOR THE MANIPULATION
		 
		Matrix matrix = new Matrix();
		 
		// RESIZE THE BIT MAP
		 
		matrix.postScale(scaleWidth, scaleHeight);
		 
		// RECREATE THE NEW BITMAP
		 
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
		 
		return resizedBitmap;
	 
	}

	public Bitmap getRoundedShape(Bitmap scaleBitmapImage, float newWidth, float newHeight) {
		  // TODO Auto-generated method stub
		  Bitmap targetBitmap = Bitmap.createBitmap((int)newWidth, (int)newHeight ,Bitmap.Config.ARGB_4444);
		  
		  Canvas canvas = new Canvas(targetBitmap);
		  Path path = new Path();
		  path.addCircle(( newWidth - 1) / 2,
		  ( newHeight - 1) / 2, (Math.min(( newWidth),  (newHeight)) / 2), Path.Direction.CCW);
		  
		  canvas.clipPath(path);
		  Bitmap sourceBitmap = scaleBitmapImage;
		  canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight()), new Rect(0, 0, (int)newWidth,
				  (int)newHeight), null);
		  
		  return targetBitmap;
	}
	public float getCorrespondenceHeight(Bitmap bm, float newWidth){
	  float newHeight = 0;
	  int width = bm.getWidth();
   	  int height = bm.getHeight();
   	  float ratio = (float)height/width;
      newHeight = newWidth*ratio;
   	  return newHeight;
	}
}
