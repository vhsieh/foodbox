package com.functionalities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import com.custom.RecyclingBitmapDrawable;
import com.entities.Global;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.utility.foodbox.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CurrentUser extends Fragment{
	
	public static final String ARG = "CurrentUser";
	protected static final int SELECT_PICTURE = 1;
	protected static final int CAMERA_REQUEST = 2;
	// directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "foodbox_img";
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;
	private View rootView;
	private Context mContext = null;
	private int screen_width = 0;
	private int screen_height = 0;
	//private boolean processToAction = true;//if no, there already exists some photos that current user set; else, we ask the next action.
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//getActivity().getActionBar().hide();
    	getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    	this.mContext = getActivity().getApplicationContext();
    	
    	
    	// inflate the root view
    	if (rootView == null) {
	    	rootView = inflater.inflate(R.layout.current_user, container, false);
	    	setScreenSize();
	    	ImageView cover = (ImageView)rootView.findViewById(R.id.user_background);
	    	LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) cover.getLayoutParams();
	    	params.height = (int) (this.screen_height*(0.3));
	    	cover.setLayoutParams(params);
	    	
	    	//Get current user and retrieve background 
	    	RetrieveImageFromParse("cover"); //check if User already sets a background image on Parse.
	    	
	    	
	    	//Set current circle photo sticker
	    	ImageView photo_sticker = (ImageView)rootView.findViewById(R.id.photo_sticker);
	    	
	    	BitmapDrawable cachedImg = Global.getBitmapFromMemCache(Global.iconUrl);
	    	if(cachedImg!=null){
	    		photo_sticker.setImageDrawable(cachedImg);
	    	}else{
	    		float targetWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, this.mContext.getResources().getDisplayMetrics());
				float targetHeight = targetWidth;
				new ImageDownloaderWithCache(this.mContext, true, targetWidth, targetHeight).download(Global.iconUrl, photo_sticker);
				
	    	}
	    	// set photo position
	    	params = (LinearLayout.LayoutParams) photo_sticker.getLayoutParams();
	    	float margin_top = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, this.mContext.getResources().getDisplayMetrics());
	    	params.setMargins(0, 0-(int)margin_top, 0, 0);
	    	//Set current user name
	    	TextView user_name = (TextView)rootView.findViewById(R.id.user_name);
	    	user_name.setText(Global.fbName);
	    	//Set view pager
	    	
	    	
    	}else{
        	ViewGroup parent = (ViewGroup) rootView.getParent();
            parent.removeView(rootView);
        }
		return rootView;
	}
	/**
     * Checking device has camera hardware or not
     * */
     private boolean isDeviceSupportCamera() {
        if (this.mContext.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
    /*
     * Choose Image Action 
     */
	public void chooseImage(){
		Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
	}
	/*
	 * Open Camera and take picture
	 */
	public void openCamera(){
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		 
	    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
	 
	    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
	 
	    // start the image capture Intent
	    startActivityForResult(intent, CAMERA_REQUEST);
	}
	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
	    return Uri.fromFile(getOutputMediaFile(type));
	}
	 
	/*
	 * returning image
	 */
	private static File getOutputMediaFile(int type) {
	 
	    // External sdcard location
	    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
	 
	    // Create the storage directory if it does not exist
	    if (!mediaStorageDir.exists()) {
	        if (!mediaStorageDir.mkdirs()) {
	            Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
	                    + IMAGE_DIRECTORY_NAME + " directory");
	            return null;
	        }
	    }
	 
	    // Create a media file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
	    }else {
	        return null;
	    }
	 
	    return mediaFile;
	}
	
	/*
	 * Sync the Cover from Facebook
	 */
	public void syncWithFacebook(){
		Bundle params = new Bundle();
		String args = "cover";
	    params.putString("fields", args);
		Request request = new Request(Session.getActiveSession(), 
		        "me/",params, HttpMethod.GET, new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						// TODO Auto-generated method stub
						FacebookRequestError error = response.getError();
						if(error!=null){
							Log.i("Error", error.getErrorMessage());
						}else{
							GraphObject graphObject = response.getGraphObject();
							if(graphObject !=null){
								JSONObject obj = graphObject.getInnerJSONObject();
								JSONObject cover_obj = obj.optJSONObject("cover");
								if(cover_obj.length()>0){
									String cover_url = cover_obj.optString("source");
									ImageView cover = (ImageView)rootView.findViewById(R.id.user_background);
							    	LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) cover.getLayoutParams();
							    	params.height = (int) (screen_height*(0.3));
							    	params.width = screen_width;
							    	cover.setLayoutParams(params);
									new ImageDownloaderWithCache(mContext, (float)screen_width,  (float)(screen_height*0.3)).download(cover_url, cover);
									
								}
								
							}
						}
					}
			
		});
		request.executeAsync();
	}
	/*
	 * Retrieve the bitmap stored in Parse
	 */
	public void RetrieveImageFromParse(String field){
		BitmapDrawable image = Global.getBitmapFromMemCache(ParseUser.getCurrentUser().getUsername());
		final ImageView img = (ImageView)rootView.findViewById(R.id.user_background);
	
		if(image != null){
			img.setImageDrawable(image);
		}
		else{
			ParseFile obj = (ParseFile) ParseUser.getCurrentUser().get("cover");
			if(obj!=null){
				Log.d(ARG, "fetch file sucessfully.");
				//processToAction = false; 
				getActivity().setProgressBarIndeterminateVisibility(true);
				obj.getDataInBackground(new GetDataCallback(){
					@Override
					public void done(byte[] data, ParseException e) {
						// TODO Auto-generated method stub
						if (e == null) {
		                    Log.d("Data", "We have data successfully");
		                    int reqHeight = (int)(screen_height*0.3);
		                    int reqWidth = screen_width;
		                    
		                    Bitmap bitmap;
		                    BitmapFactory.Options opts = new BitmapFactory.Options();
		                    opts.inJustDecodeBounds = true;
		                    BitmapFactory.decodeByteArray(data, 0, data.length);
		                    final int height = opts.outHeight;
		            	    final int width = opts.outWidth;
		            	    int inSampleSize = 1;
		            	
		            	    if (height > reqHeight || width > reqWidth) {
		            	
		            	        final int halfHeight = height / 2;
		            	        final int halfWidth = width / 2;
		            	
		            	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
		            	        // height and width larger than the requested height and width.
		            	        while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
		            	            inSampleSize *= 2;
		            	        }
		            	    }
		                    // Load pre-scaled bitmap
		                    opts = new BitmapFactory.Options();
		                    opts.inSampleSize = inSampleSize;
		                    bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		                    
		                    // add to memory cache
		                    RecyclingBitmapDrawable rbd = new RecyclingBitmapDrawable(mContext.getResources(), bitmap);
		                    Global.mMemoryCache.remove(ParseUser.getCurrentUser().getUsername());
		                    Global.addBitmapToMemoryCache(ParseUser.getCurrentUser().getUsername(), rbd);
		                    rbd.setIsCached(true);
		                    
		                    img.setImageBitmap(bitmap);
		                    getActivity().setProgressBarIndeterminateVisibility(false);
						}else{
							Log.d("ERROR: ", "" + e.getMessage());
						}
					}
					
				});
			}else{
				Log.d(ARG, "no data");
			}
			img.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					CharSequence actions[] = new CharSequence[] {"Choose from Gallery", "Take photo", "Cancel"};
	
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("options");
					builder.setCancelable(true);
					builder.setItems(actions, new DialogInterface.OnClickListener() {
					    @Override
					    public void onClick(DialogInterface dialog, int which) {
					        switch(which){
					        	case 0:
					        		chooseImage();
					        		break;
					        	case 1:
					        		// do we have a camera?
					        	    if (isDeviceSupportCamera()) {
					        	        openCamera();
					        	    }
					        		break;
					        	case 2:
					        		dialog.dismiss();
					        		break;
					        }
					    }
					});
					builder.show();
					// TODO Auto-generated method stub
				}
			});
		}
		
	}
	@SuppressWarnings("deprecation")
	private void setScreenSize(){
    	//get screen width
    	Display display = getActivity().getWindowManager().getDefaultDisplay(); 

    	this.screen_width = display.getWidth();
    	this.screen_height = display.getHeight();
    }
	//UPDATED
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
        	String image_path = null;
            if (requestCode == SELECT_PICTURE) {
            	image_path = getPath(data.getData());
            }else if(requestCode == CAMERA_REQUEST){
            	image_path =  fileUri.getPath();
            }
            if(image_path!=null)
            	displayImage(image_path);
            else
            	System.out.println(data.getDataString());
        }
    }
    /*
     * After choosing image or take photos, display the image in ImageView (take care of memony issiue)
     */
    private void displayImage(String path){
    	final int IMAGE_MAX_SIZE = 300;
        try {
        	System.out.println("path:"+path);
            // Bitmap bitmap;
            File file = null;
            FileInputStream fis;
            BitmapFactory.Options opts;
            int resizeScale;
            
            file = new File(path);
            
            // This bit determines only the width/height of the
            // bitmap
            // without loading the contents
            opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            fis = new FileInputStream(file);
            BitmapFactory.decodeStream(fis, null, opts);
            fis.close();

            // Find the correct scale value. It should be a power of
            // 2
            resizeScale = 1;

            if (opts.outHeight > IMAGE_MAX_SIZE || opts.outWidth > IMAGE_MAX_SIZE) {
                resizeScale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE/ (double) Math.max(opts.outHeight, opts.outWidth)) / Math.log(0.5)));
            }

            // Load pre-scaled bitmap
            opts = new BitmapFactory.Options();
            opts.inSampleSize = resizeScale;
            
            fis = new FileInputStream(file);
            Bitmap bmp = BitmapFactory.decodeStream(fis, null, opts);
            ImageView cover = (ImageView)rootView.findViewById(R.id.user_background);
	    	LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) cover.getLayoutParams();
	    	params.height = (int) (screen_height*(0.3));
	    	params.width = screen_width;
	    	cover.setLayoutParams(params);
	    	cover.setImageBitmap(bmp);
            // add to memory cache
            RecyclingBitmapDrawable rbd = new RecyclingBitmapDrawable(mContext.getResources(), bmp);
            Global.mMemoryCache.remove(ParseUser.getCurrentUser().getUsername());
            Global.addBitmapToMemoryCache(ParseUser.getCurrentUser().getUsername(), rbd);
            rbd.setIsCached(true);
            
            // save to Parse for future use
            new Thread(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					storeBitmapToParse("cover", R.id.user_background);
				}
            	
            }).start();
            
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    //UPDATED!
    public String getRealPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = mContext.getContentResolver().query(contentUri, proj, null, null, null);
        if(cursor.moveToFirst()){;
           int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
           res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
            // just some safety built in 
            if( uri == null ) {
                // TODO perform some logging or show user feedback
                return null;
            }
            // try to retrieve the image from the media store first
            // this will only work for images selected from gallery
            String[] projection = { MediaStore.Images.Media.DATA };
            Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
            if( cursor != null ){
                int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
            // this is our fallback here
            return uri.getPath();
    }
    /*
     * After retreving required image, store it to Parse for convenience.
     *@param: field, the field in Parse
     */
    public void storeBitmapToParse(final String field, int imageView_id){
    	System.out.println("saving");
    	//retrieve image from ImageView by ID
    	ImageView view = (ImageView)rootView.findViewById(imageView_id);
    	final Bitmap bitmap = ((BitmapDrawable) view.getDrawable()).getBitmap();
    	
    	System.out.println("Get bitmap:"+bitmap);
    	// store to parse
    	if(bitmap!=null){
    		ByteArrayOutputStream blob = new ByteArrayOutputStream(); 
    		
    		bitmap.compress(CompressFormat.PNG, 30, blob); 
    		byte[] data = blob.toByteArray();
    		final ParseFile file = new ParseFile(data);
    		file.saveInBackground(new SaveCallback(){

				@Override
				public void done(ParseException e) {
					// TODO Auto-generated method stub
					if(e==null){
						Log.d(ARG, "file ok, start saving");
		    	    	ParseUser user = ParseUser.getCurrentUser();
		    	    	user.put(field, file);
		    	    	user.saveInBackground(new SaveCallback(){

							@Override
							public void done(ParseException e) {
								// TODO Auto-generated method stub
								if(e==null){
									Log.d(ARG, "save to parse!");
									// add to memory cache
						            RecyclingBitmapDrawable rbd = new RecyclingBitmapDrawable(mContext.getResources(), bitmap);
						            Global.mMemoryCache.remove(ParseUser.getCurrentUser().getUsername());
						            Global.addBitmapToMemoryCache(ParseUser.getCurrentUser().getUsername(), rbd);
						            rbd.setIsCached(true);
								}else{
									Log.d(ARG, "some error occur:"+e.getMessage());
								}
							}
		    	    		
		    	    	});
					}else{
						Log.d(ARG, e.getMessage());
					}
				}
    			
    		});
    	    	
    	}
    }
    @Override
    public void onPause()
    {
      super.onPause();
      System.out.println("onPause called");
      System.gc();
    }
    @Override
    public void onStop(){
    	super.onStop();
    	Global.mMemoryCache.evictAll();
    }
    @Override
    public void onDestroy()
    {
      super.onDestroy();
      System.gc();
    }
}
