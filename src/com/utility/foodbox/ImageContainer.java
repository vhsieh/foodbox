package com.utility.foodbox;


import com.custom.ImageContainerAdapter;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ListView;

public class ImageContainer extends Activity {
	private ImageContainerAdapter adapter = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//get screen width
		
		String[] urls = getIntent().getExtras().getStringArray("urls");//.getString("url").toString();
		String[] owners = getIntent().getExtras().getStringArray("owners");
		getActionBar().hide();
		setContentView(R.layout.image_container);
		
		adapter = new ImageContainerAdapter(this.getApplicationContext(), android.R.layout.simple_list_item_1, urls, owners);
		ListView container = (ListView)findViewById(R.id.container);
		container.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.single_image, menu);
		return true;
	}

}
