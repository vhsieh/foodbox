package com.utility.foodbox;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;

@SuppressLint("NewApi")
public class MapActivity extends Activity {
	private GoogleMap mMap;
	private double lat;
	private double lng;
	private String title;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_actitivy);
		getActionBar().hide();
		lat =  getIntent().getExtras().getDouble("lat");
		lng =  getIntent().getExtras().getDouble("lng");
		title = getIntent().getExtras().getString("title");
		setUpMapIfNeeded();
		
		
	}
	private void setUpMapIfNeeded() {
	    // Do a null check to confirm that we have not already instantiated the map.
	    if (mMap == null) {
	        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
	                            .getMap();
	        // Check if we were successful in obtaining the map.
	        if (mMap != null) {
	            // The Map is verified. It is now safe to manipulate the map.
	        	LatLng point = new LatLng(this.lat, this.lng);
	        	mMap.addMarker(new MarkerOptions().position(point).title(title));
	        	 // Move the camera instantly to hamburg with a zoom of 15.
	            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 15));

	            // Zoom in, animating the camera.
	            mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
	        }
	    }
	}
}
