/**
 * 
 */
package com.utility.foodbox;



import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.utility.foodbox.MainFragment;


/**
 * @author vincent732
 *
 */
public class MainActivity extends FragmentActivity {
	private MainFragment mainFragment;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.main);
		/*
		 * need to define different sizes of screens 
		 */
	    if (savedInstanceState == null) {
	        // Add the fragment on initial activity setup
	        mainFragment = new MainFragment();
	        getSupportFragmentManager()
	        .beginTransaction()
	        .add(android.R.id.content, mainFragment)
	        .commit();
	    } else {
	        // Or set the fragment from restored state info
	        mainFragment = (MainFragment) getSupportFragmentManager()
	        .findFragmentById(android.R.id.content);
	    }
	    
	}
}
