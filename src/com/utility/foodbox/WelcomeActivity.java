package com.utility.foodbox;


import java.util.Timer;
import java.util.TimerTask;

import com.facebook.Session;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.Menu;
import android.widget.RelativeLayout;

public class WelcomeActivity extends Activity {
	
	private RelativeLayout BackgroundPanel = null;
	private BitmapDrawable bmpDrawImg  = null;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_welcome);
		BackgroundPanel = (RelativeLayout)findViewById(R.id.login_background);
		bmpDrawImg = new BitmapDrawable(getResources().openRawResource(R.drawable.welcome));
		BackgroundPanel.setBackgroundDrawable(bmpDrawImg);
		
		//check network connection
		if(isInternetConnectionActive(this)){
				// Auto switch to main activity after 3 seconds 
				//Initite Parse & switch to next activity
				ToNextActivity();
		}else
        {
				AlertDialog dialogBox = makeAndShowDialogBox();
				dialogBox.show();
        }
		
	}
	
	private void ToNextActivity(){
		// Auto switch to main activity after 3 seconds 
		//Initiate Parse 
		if(!isInternetConnectionActive(this)){
			AlertDialog dialogBox = makeAndShowDialogBox();
			dialogBox.show();
		}
		TimerTask task = new TimerTask(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Session session = Session.getActiveSession();
				//restart 
				if(session == null){
					session = Session.openActiveSessionFromCache(getApplicationContext());
				}
				
				
				if(session != null){ //(Session.getActiveSession() != null || Session.getActiveSession().isOpened()){
			    	Log.i("SessionToken", session.getAccessToken());
			    	Log.i("SessionDueDate", session.getExpirationDate().toString());
			    	Intent i = new Intent(WelcomeActivity.this, ContentActivity.class);
			    	startActivity(i);
			    	WelcomeActivity.this.finish();
			    }
				else{
					Intent next = new Intent();
					next.setClass(WelcomeActivity.this, Login.class);
					startActivity(next);
					
					WelcomeActivity.this.finish();
				}
			}
		};
		Timer t = new Timer();
		t.schedule(task, 3000);
	}
	public static boolean isInternetConnectionActive(Context context) {
		ConnectivityManager CM = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = CM.getActiveNetworkInfo();
		  if(info == null || !info.isConnected()) {
		   return false;
		  }
		  return true;
	}
	
	/*Prompt user to retry or exit
	 * 
	 * */
	private AlertDialog makeAndShowDialogBox()
    {
        AlertDialog myQuittingDialogBox =  new AlertDialog.Builder(this) 

            .setCancelable(false)
            .setTitle("Network Problem")
            .setMessage("You are not connected to any network...") 

            .setPositiveButton("Retry", new DialogInterface.OnClickListener() { 
                public void onClick(DialogInterface dialog, int whichButton) { 
                 //whatever should be done when answering "YES" goes here
                    if(isInternetConnectionActive(WelcomeActivity.this)){
                    	ToNextActivity();
                    }
                }              
            })//setPositiveButton
            .setNegativeButton("Exit", new DialogInterface.OnClickListener() { 
                public void onClick(DialogInterface dialog, int whichButton) { 
                 //whatever should be done when answering "NO" goes here
                    WelcomeActivity.this.finish();
             } 
            })//setNegativeButton

            .create();
       return myQuittingDialogBox;
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.welcome, menu);
		return true;
	}
	@Override
	public void onDestroy(){
		super.onDestroy();
		BackgroundPanel.getBackground().setCallback(null);
		if(bmpDrawImg!=null && !bmpDrawImg.getBitmap().isRecycled()){
			bmpDrawImg.getBitmap().recycle();
		}
	}
}
