package com.utility.foodbox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.json.JSONObject;
import org.json.JSONTokener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.entities.Global;
import com.entities.foursquare.FoursquareSession;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.foursquare.android.nativeoauth.FoursquareOAuth;
import com.foursquare.android.nativeoauth.model.AuthCodeResponse;
import com.functionalities.ImageDownloaderWithCache;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

@SuppressLint({ "NewApi", "SimpleDateFormat" })
public class Login extends Activity {

	private ImageView fb;
	private ImageView foursquare;
	private Dialog progressDialog;
	// parameters for foursquare
	private int foursquareResultCode = 1;
	private String accessUrl;
	private static final String API_URL = "https://api.foursquare.com/v2";
	private String TAG = "Login";
	private String access_token;
	private FoursquareSession mSession;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.login);
		
		//Facebook
		fb = (ImageView)findViewById(R.id.connectFB);
		fb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onfbClicked();
			}
		});
		//Foursquare
		mSession = new FoursquareSession(this);
		foursquare = (ImageView)findViewById(R.id.connectFourSquare);
		foursquare.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onFoursquareClicked();
			}
		});
		
		ImageView next = (ImageView)findViewById(R.id.next);
		ParseUser currentUser = ParseUser.getCurrentUser();
		next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ParseUser user = ParseUser.getCurrentUser();
				if(user != null)
					ToNextActivity();
			}
		});
		
		
		// Check if there is a currently logged in user
		// and they are linked to a Facebook account.
		
		if ((currentUser != null) && ParseFacebookUtils.isLinked(currentUser)) {
			
			SetFacebookImage();
			// Go to the user info activity
			ToNextActivity();
			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(TAG, String.valueOf(requestCode));
		Log.d(TAG, String.valueOf(resultCode));
		if(requestCode==1){
			AuthCodeResponse codeResponse = FoursquareOAuth.getAuthCodeFromResult(resultCode, data);
			if(codeResponse.getException()==null){
				String accessCode = codeResponse.getCode();
				String client_id = getResources().getString(R.string.foursquare_client_id);
				String client_secrect = getResources().getString(R.string.foursquare_client_secrect);
				accessUrl = "https://foursquare.com/oauth2/access_token?client_id="+client_id+"&client_secret="+client_secrect+"&grant_type=authorization_code&code="+accessCode;
				getAccessToken();
			}else{
				Log.d(TAG, codeResponse.getException().getMessage());
			}
				
		}//foursquare
		else{
			ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
		}
	}
	/*
	 * Get access token from Foursquare 
	 */
	private void onFoursquareClicked(){
		Intent intent = FoursquareOAuth.getConnectIntent(this, getResources().getString(R.string.foursquare_client_id));
		startActivityForResult(intent, foursquareResultCode);
		
	}
	
	private void onfbClicked() {
		Login.this.progressDialog = ProgressDialog.show(
				Login.this, "", "Logging in...", true);
		List<String> permissions = Arrays.asList("basic_info", "user_about_me",
				"user_relationships", "user_birthday", "user_location", "user_photos",
				"friends_photos", "user_status", "friends_status", "friends_location",
				"friends_status", "friends_checkins","friends_activities", "friends_likes");
		ParseFacebookUtils.logIn(permissions, this, new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException err) {
				Login.this.progressDialog.dismiss();
				if (user == null) {
					Log.d(TAG,
							"Uh oh. The user cancelled the Facebook login.");
				} else if (user.isNew()) {
					Log.d(TAG,
							"User signed up and logged in through Facebook!");
					SetFacebookImage();
				} else {
					Log.d(TAG,
							"User logged in through Facebook!");
					SetFacebookImage();
				}
			}
		});
	}
	private void SetFacebookImage(){
		 String args = "picture.type(large), name";
		 Bundle params = new Bundle();
	     params.putString("fields", args);
		 Request request = new Request(Session.getActiveSession(), 
		        "me/",params, HttpMethod.GET, new Request.Callback() {
				@Override
				public void onCompleted(Response response) {
					// TODO Auto-generated method stub
					FacebookRequestError error = response.getError();
					if(error!=null){
						Log.i("Error", error.getErrorMessage());
					}else{
						GraphObject graphObject = response.getGraphObject();
						if(graphObject !=null){
							JSONObject obj = graphObject.getInnerJSONObject();
							String name = obj.optString("name");
							JSONObject url_obj = obj.optJSONObject("picture");
							Global.fbName = name;
							if(url_obj!=null){
								String url = url_obj.optJSONObject("data").optString("url");
								Global.iconUrl = url;
								float targetWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, getApplication().getApplicationContext().getResources().getDisplayMetrics());
								float targetHeight = targetWidth;
								new ImageDownloaderWithCache(getApplicationContext(), true, targetWidth, targetHeight).download(url, fb);
								findViewById(R.id.next).setVisibility(View.VISIBLE);
							}
							
						}
					}
				}
		 });
		
		 request.executeAsync();
	}
	private void SetFoursquareImage(final String image_url){
		Handler photo = new Handler(Looper.getMainLooper());
		photo.post(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				float targetWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, getApplication().getApplicationContext().getResources().getDisplayMetrics());
				float targetHeight = targetWidth;
				new ImageDownloaderWithCache(getApplicationContext(), true, targetWidth, targetHeight).download(image_url, foursquare);
				findViewById(R.id.next).setVisibility(View.VISIBLE);
			}
			
		});
	}
	@SuppressWarnings("deprecation")
	private void ToNextActivity(){
	  final ArrayList<String> FBID = new ArrayList<String>();
	  Request.executeMyFriendsRequestAsync(Session.getActiveSession(), new Request.GraphUserListCallback() {
		
		@Override
		public void onCompleted(final List<GraphUser> users, final Response response) {
			// TODO Auto-generated method stub
			if(users!=null){
				for(final GraphUser user:users){
					FBID.add(user.getId());
				}
				Global.FBIDs = FBID;
				final Intent intent = new Intent(Login.this, ContentActivity.class);
				
				startActivity(intent);
				Login.this.finish();
			}
			
		}
	});
	  
	}
	/*
	 *  Foursquare functions
	 * 
	 */
	private void getAccessToken() {
		new Thread() {
			@Override
			public void run() {
				Log.i(TAG, "Getting access token");

				int what = 0;

				try {
					URL url = new URL(accessUrl);

					Log.i(TAG, "Opening URL " + url.toString());

					HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

					urlConnection.setRequestMethod("GET");
					urlConnection.setDoInput(true);
					//urlConnection.setDoOutput(true);

					urlConnection.connect();

					JSONObject jsonObj  = (JSONObject) new JSONTokener(streamToString(urlConnection.getInputStream())).nextValue();
		        	access_token 		= jsonObj.getString("access_token");
		        	SharedPreferences sharedPreferences = getSharedPreferences("fqAccessToken", 0);
		        	Editor editor = sharedPreferences.edit();
		        	editor.putString("access_token", access_token);
		        	editor.commit();
		        	Log.i(TAG, "Got access token: " + access_token);
				} catch (Exception ex) {
					what = 1;

					ex.printStackTrace();
				}

				mHandler.sendMessage(mHandler.obtainMessage(what, 1, 0));
			}
		}.start();
	}

	private void fetchUserName() {
		new Thread() {
			@Override
			public void run() {
				Log.i(TAG, "Fetching user name");
				int what = 0;

				try {
					String v	= timeMilisToString(System.currentTimeMillis()); 
					URL url 	= new URL(API_URL + "/users/self?oauth_token=" + access_token + "&v=" + v);

					Log.d(TAG, "Opening URL " + url.toString());

					HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

					urlConnection.setRequestMethod("GET");
					urlConnection.setDoInput(true);
					//urlConnection.setDoOutput(true);

					urlConnection.connect();

					String response		= streamToString(urlConnection.getInputStream());
					JSONObject jsonObj 	= (JSONObject) new JSONTokener(response).nextValue();

					JSONObject resp		= (JSONObject) jsonObj.get("response");
					JSONObject user		= (JSONObject) resp.get("user");
					
					String firstName 	= user.optString("firstName");
		        	//String lastName		= user.optString("lastName");
		        	

		        	
		        	//get user image
		        	
		        	JSONObject photo = user.optJSONObject("photo");
		        	String prefix = photo.optString("prefix");
		        	String suffix = photo.optString("suffix");
		        	if(prefix!=null && suffix!=null){
		        		prefix = prefix.replace("\\", "");
		        		suffix = suffix.replace("\\//", "");
		        		String image_url = prefix+"original"+suffix;
		        		SetFoursquareImage(image_url);
		        		
		        	}
		        	Log.d(TAG, access_token);
		        	Log.d(TAG, firstName);
		        	mSession.storeAccessToken(access_token, firstName);
		        	
				} catch (Exception ex) {
					what = 1;

					ex.printStackTrace();
				}

				mHandler.sendMessage(mHandler.obtainMessage(what, 2, 0));
			}
		}.start();
	}

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.arg1 == 1) {
				if (msg.what == 0) {
					fetchUserName();
				} else {
					Log.d(TAG, "Failed to get access token");
				}
			} else {
				Log.d(TAG, "Success to get access token");
			}
			
		}
	};
	private String streamToString(InputStream is) throws IOException {
		String str  = "";

		if (is != null) {
			StringBuilder sb = new StringBuilder();
			String line;

			try {
				BufferedReader reader 	= new BufferedReader(new InputStreamReader(is));

				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				reader.close();
			} finally {
				is.close();
			}

			str = sb.toString();
		}

		return str;
	}

	private String timeMilisToString(long milis) {
		SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar   = Calendar.getInstance();

		calendar.setTimeInMillis(milis);

		return sd.format(calendar.getTime());
	}
	
}