package com.utility.foodbox;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.entities.Global;
import com.entities.Restaurant;
import com.functionalities.CurrentUser;
import com.functionalities.GPSTracker;
import com.functionalities.ToEatList;
import com.functionalities.home;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.custom.AddNewListDialogFragment;
import com.custom.LeftDrawerAdapter;
@SuppressLint({ "UseValueOf", "InlinedApi", "NewApi" })
public class ContentActivity extends FragmentActivity implements AddNewListDialogFragment.AddNewListDialogListener{
	//private SelectionFragment selectFragment;
	public static final String TAG = "ContentActivity";
	private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mPlanetTitles;
    // GPSTracker class
    GPSTracker gps;
    
    
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		super.onCreate(savedInstanceState);
		this.getApplicationContext();
		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		PushService.setDefaultPushCallback(this, HomeFragment.class);
		ParseAnalytics.trackAppOpened(getIntent());
		// set up notification environment
    	ParseInstallation installation = ParseInstallation.getCurrentInstallation();
    	installation.put("user", ParseUser.getCurrentUser());
		installation.saveInBackground();
		this.setContentView(R.layout.activity_content);
		
		//Get GPS location
    	gps = new GPSTracker(ContentActivity.this);
    	// check if GPS enabled     
    	if(gps.canGetLocation()){
    		  Global.latitude = gps.getLatitude();
    		  Global.longitude = gps.getLongitude();
    	}else{
    		 /*can't get location
    		  GPS or Network is not enabled
    		  ask user to enable GPS/network in settings*/
              gps.showSettingsAlert();
    	}
    	
		//Restore preferences
		if(Global.FBIDs == null  || Global.fbName == null || Global.iconUrl== null){
		    SharedPreferences settings = getSharedPreferences("UserInfo", 0);
		    Set<String> fbIDs= (Set<String>) settings.getStringSet("FBID", null);
			for(String id:fbIDs){
				Global.FBIDs.add(id);
			}
			Global.fbName = settings.getString("name", null);
			Global.iconUrl = settings.getString("url", null);
		    
		}
		
		/*
         * check if Location Service is started..
         * 
         */
    	
    	/*Intent service = new Intent(cxt, LocationService.class);
    	if(startService(service) != null) { 
    		Log.d(TAG, "Service is already running");
    	}else {
    		Log.d(TAG, "There is no service running, starting service..");
    	}*/
		
    	
		//set content
		String[] functionalities = getResources().getStringArray(R.array.functionalities);
		List<String> temp = new ArrayList<String>(Arrays.asList(functionalities));
		temp.add(0, Global.fbName);
		functionalities = temp.toArray(new String[temp.size()]);
		
		
		mPlanetTitles = functionalities;//getResources().getStringArray(R.array.functionalities);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        //mDrawerList.setAdapter(new ArrayAdapter<String>(this,
        //        R.layout.drawer_list_item, mPlanetTitles));
        mDrawerList.setAdapter(new LeftDrawerAdapter(this, R.layout.drawer_list_item, mPlanetTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        
        mDrawerList.setBackgroundColor(this.getResources().getColor(R.color.pure_white));
        // enable ActionBar app icon to behave as action to toggle nav drawer
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getActionBar().setHomeButtonEnabled(true);
        
        this.getActionBar().setBackgroundDrawable(this.getResources().getDrawable(R.drawable.action_bar_background));
        
        
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,      /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
        if (savedInstanceState == null) {
        	
            selectItem(1);
            
        }

        
	}

	@Override
	public void onResume(){
		Log.d(TAG, "onResume called");
		
		super.onResume();
	}
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
        /*
        // Handle action buttons
        switch(item.getItemId()) {
        case R.id.action_websearch:
            // create intent to perform web search for this planet
            Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
            // catch event that there's no activity to handle intent
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
            }
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }*/
    }
	
    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			selectItem(position);
			
		}
    }

	private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        Bundle args = new Bundle();
        switch(position){
        	case 0:
        		fragment = new CurrentUser();
        		args.putInt(CurrentUser.ARG, position);
        		break;
        	case 1:
        		fragment = new home();
        		args.putInt(home.ARG, position);
        		break;
        	case 2:
        		fragment = new ToEatList();
        		break;
        }
        
        fragment.setArguments(args);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mPlanetTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
    protected void onStop(){
    	super.onStop();
    	System.gc();
    }

    @Override
    public void onPause()
    {
    	super.onPause();
    	SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        SharedPreferences.Editor editor = settings.edit();
        //set FB friend list
        Set<String> set = new HashSet<String>();
        set.addAll(Global.FBIDs);
        
        editor.putStringSet("FBID", set);
        
        //Save user name
        editor.putString("name", Global.fbName);
        
        //Save FB icon url
        editor.putString("url", Global.iconUrl);
        
        
        //Save Restaurants
        Set<Restaurant> restaurant = new HashSet<Restaurant>();
        restaurant.addAll(Global.restaurants);
        
        
        editor.commit();
      
    }
    @Override
    public void onDestroy()
    {
      super.onDestroy();
      
      System.gc();
    }

	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		Fragment fragment = new ToEatList();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
}
