package com.utility.foodbox;

import java.util.List;

import com.custom.AddNewListDialogFragment;
import com.custom.RestaurantPagerAdapter;
import com.entities.Global;
import com.entities.Restaurant;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;

public class HomeFragment extends FragmentActivity implements AddNewListDialogFragment.AddNewListDialogListener{
	public static final String TAG = "HomeFragment";
	RestaurantPagerAdapter mRestaurantPagerAdapter;
    ViewPager mViewPager; 
    PagerTabStrip mPagerTabStrip;
    private List<Restaurant> values;
    private int current;
    Context cxt;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate called");
		
		cxt = this.getApplicationContext();
		getActionBar().hide();
    	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.home_fragment);
		// ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
		Bundle extras = getIntent().getExtras();
		values = Global.restaurants;
		current = (int)extras.getInt("current");
		
		mRestaurantPagerAdapter = new RestaurantPagerAdapter(getSupportFragmentManager(), values);
		
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mRestaurantPagerAdapter);
        mViewPager.setCurrentItem(current);
        
        mPagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_tab_strip);
        mPagerTabStrip.setDrawFullUnderline(true);
        mPagerTabStrip.setTabIndicatorColor(getResources().getColor(R.color.black));
        mPagerTabStrip.setBackgroundColor(getResources().getColor(R.color.dark_red));
        mPagerTabStrip.setNonPrimaryAlpha(0.5f);
        mPagerTabStrip.setTextSpacing(25);
        mPagerTabStrip.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        
	}
	@Override
	public void onResume(){
		Log.d(TAG, "onResume called");
		super.onResume();
	}
	@Override
    public void onPause(){
		Log.d(TAG, "onPause called");
		super.onPause();
		
    }
	@Override
	public void onStop(){
		Log.d(TAG, "onStop called");
        super.onStop();
        
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_fragment, menu);
		return true;
	}
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}

}
