package com.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootBroadcastReceiver extends BroadcastReceiver{
	static final String ACTION = "android.intent.action.BOOT_COMPLETED";
	@Override
	public void onReceive(Context context, Intent i) {
		// TODO Auto-generated method stub
		
		Log.d("BootBroadcastReceiver", i.getAction());
		if(i.getAction().equals(ACTION)){
			Log.d("BootBroadcastReceiver", i.getAction());
			Intent service = new Intent(context, LocationService.class);
			context.startService(service);
			
		}
	}

}
