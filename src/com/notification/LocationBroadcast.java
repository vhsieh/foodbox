package com.notification;

import java.util.List;
import java.util.Set;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class LocationBroadcast extends BroadcastReceiver{
	private static final String TAG = "LocationBroadcast";
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if(intent.getAction().equals("Location Service")){
			double latitude = intent.getExtras().getDouble("Latitude");
			double longitude = intent.getExtras().getDouble("Longitude");
			final Set<String> setOfAllSubscriptions = PushService.getSubscriptions(context.getApplicationContext());
			
			ParseQuery<ParseObject> pq = ParseQuery.getQuery("restaurant");
			pq.whereWithinKilometers("point", new ParseGeoPoint(latitude, longitude), 0.5);
			pq.findInBackground(new FindCallback<ParseObject>(){

				@Override
				public void done(List<ParseObject> response, ParseException e) {
					// TODO Auto-generated method stub
					if(e==null){
						for(ParseObject p:response){
							String object_id = "a"+p.getObjectId();
							Log.d(TAG, object_id);
							if(setOfAllSubscriptions.contains(object_id)){
								Log.d(TAG, "send notification");
								sendNotification(p.get("name").toString(), object_id);
							}
						}
					}
				}
			});
			
		}
	}
	public void sendNotification(String name, String object_id){
		ParseQuery<ParseInstallation> pushQuery = ParseInstallation.getQuery();
		//pushQuery.whereContains("channels", object_id);
		pushQuery.whereEqualTo("user", ParseUser.getCurrentUser());
		ParsePush push = new ParsePush();
		push.setQuery(pushQuery);
		push.setMessage(name+" is approaching!");
		push.sendInBackground();
	}
	

}
